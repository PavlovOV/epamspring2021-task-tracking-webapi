﻿using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Repositories
{
	class ProjectRepository : IRepository<Project>
	{
		private readonly TaskTrackingDbContext _db;

		public ProjectRepository(TaskTrackingDbContext db)
		{
			_db = db;
		}
		public void Add(Project entity)
		{
			_db.Projects.Add(entity);
		}

		public IQueryable<Project> FindAll()
		{
			return _db.Projects;
		}

		public IQueryable<Project> FindAllWithDetails()
		{
			return _db.Projects.Include(p => p.ApplicationUser);
		}

		public async Task<Project> GetByIdAsync(int id)
		{
			return await _db.Projects.FindAsync(id);
		}

		public async Task<Project> GetByIdWithDetailsAsync(int id)
		{
			return await _db.Projects.Include(p => p.ApplicationUser)
				.FirstOrDefaultAsync(i => i.Id == id);
		}
		public void Update(Project entity)
		{
			_db.Projects.Update(entity);
		}
	}
}
