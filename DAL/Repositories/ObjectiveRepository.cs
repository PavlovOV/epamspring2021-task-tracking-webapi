﻿using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Repositories
{
	public class ObjectiveRepository : IRepository<Objective>
	{
		private readonly TaskTrackingDbContext _db;

		public ObjectiveRepository(TaskTrackingDbContext db)
		{
			_db = db;
		}
		public void Add(Objective entity)
		{
			_db.Objectives.Add(entity);
		}


		public IQueryable<Objective> FindAll()
		{
			return _db.Objectives;
		}

		public IQueryable<Objective> FindAllWithDetails()
		{
			return _db.Objectives
				.Include(p => p.ApplicationUser)
				.Include(p => p.Status)
				.Include(p => p.Project)
				.Include(p=>p.WorkLogs);
		}

		public async Task<Objective> GetByIdAsync(int id)
		{
			return await _db.Objectives.FindAsync(id);
		}

		public async Task<Objective> GetByIdWithDetailsAsync(int id)
		{
			return await _db.Objectives
				.Include(p => p.ApplicationUser)
				.Include(p => p.Status)
				.Include(p => p.Project)
				.Include(p => p.WorkLogs)
				.FirstOrDefaultAsync(i => i.Id == id);
		}
		public void Update(Objective entity)
		{
			_db.Objectives.Update(entity);
		}
	}
}
