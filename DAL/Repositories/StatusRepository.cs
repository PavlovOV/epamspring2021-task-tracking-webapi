﻿using DAL.Entities;
using DAL.Interfaces;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Repositories
{
	public class StatusRepository : IReadonlyRepository<Status>
	{
		private readonly TaskTrackingDbContext _db;

		public StatusRepository(TaskTrackingDbContext db)
		{
			_db = db;
		}

		public IQueryable<Status> FindAll()
		{
			return _db.Statuses;
		}

		public async Task<Status> GetByIdAsync(int id)
		{
			return await _db.Statuses.FindAsync(id);
		}
	}
}
