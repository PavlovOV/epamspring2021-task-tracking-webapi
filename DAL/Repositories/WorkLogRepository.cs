﻿using DAL.Entities;
using DAL.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Repositories
{
	public class WorkLogRepository : IRepository<WorkLog>
	{
		private readonly TaskTrackingDbContext _db;

		public WorkLogRepository(TaskTrackingDbContext db)
		{
			_db = db;
		}
		public void Add(WorkLog entity)
		{
			_db.WorkLogs.Add(entity);
		}


		public IQueryable<WorkLog> FindAll()
		{
			return _db.WorkLogs;
		}

		public IQueryable<WorkLog> FindAllWithDetails()
		{
			throw new NotImplementedException();
		}

		public Task<WorkLog> GetByIdAsync(int id)
		{
			throw new NotImplementedException();
		}

		public Task<WorkLog> GetByIdWithDetailsAsync(int id)
		{
			throw new NotImplementedException();
		}
		public void Update(WorkLog entity)
		{
			_db.WorkLogs.Update(entity);
		}
	}
}
