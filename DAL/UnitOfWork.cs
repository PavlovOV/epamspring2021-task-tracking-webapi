﻿using DAL.Entities;
using DAL.Interfaces;
using DAL.Repositories;
using Microsoft.AspNetCore.Identity;
using System.Linq;
using System.Threading.Tasks;

namespace DAL
{

	public class UnitOfWork : IUnitOfWork
	{
		private readonly TaskTrackingDbContext _db;

		readonly IRepository<Project> _projects;
		readonly IRepository<Objective> _objectives;
		readonly IReadonlyRepository<Status> _statuses;
		readonly IRepository<WorkLog> _workLogs;

		public UnitOfWork(TaskTrackingDbContext db)
		{
			_db = db;
			_projects = new ProjectRepository(_db);
			_objectives = new ObjectiveRepository(_db);
			_statuses = new StatusRepository(_db);
			_workLogs = new WorkLogRepository(_db);
		}
		public IRepository<Project> Projects { get { return _projects; } }
		public IRepository<Objective> Objectives { get { return _objectives; } }
		public IRepository<WorkLog> WorkLogs { get { return _workLogs; } }
		public IReadonlyRepository<Status> Statuses { get { return _statuses; } }
		public IQueryable<IdentityUserRole<string>> UserRoles { get { return _db.UserRoles; } }
		public async Task<int> SaveAsync()
		{
			return await _db.SaveChangesAsync();
		}
	}
}
