﻿using DAL.Entities;
using DAL.Interfaces;
using Microsoft.AspNetCore.Identity;
using System.Linq;
using System.Threading.Tasks;

namespace DAL
{
	public interface IUnitOfWork
	{
		IRepository<Project> Projects { get; }
		IRepository<Objective> Objectives { get; }
		IRepository<WorkLog> WorkLogs { get; }
		IReadonlyRepository<Status> Statuses { get; }

		IQueryable<IdentityUserRole<string>> UserRoles { get; }

		Task<int> SaveAsync();
	}
}
