﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace DAL.Entities
{
	public class ApplicationUser : IdentityUser
	{
		public virtual ICollection<Project> Projects { get; set; }
		public virtual ICollection<Objective> Objectives { get; set; }
	}
}
