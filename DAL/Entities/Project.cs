﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.Entities
{
	public class Project : BaseEntity
	{
		public string Name { get; set; }
		public string Description { get; set; }
		public string UserId { get; set; }

		[ForeignKey("UserId")]
		public virtual ApplicationUser ApplicationUser { get; set; }
	}
}
