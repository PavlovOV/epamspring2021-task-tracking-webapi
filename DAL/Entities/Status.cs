﻿
using System.Collections.Generic;

namespace DAL.Entities
{
	public class Status : BaseEntity
	{
		public string Name { get; set; }
		public virtual ICollection<Objective> Objectives { get; set; }
	}
}
