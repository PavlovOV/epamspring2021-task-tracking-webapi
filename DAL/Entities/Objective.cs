﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.Entities
{
	public class Objective : BaseEntity
	{
		public string Name { get; set; }
		public string Description { get; set; }
		public int StatusId { get; set; }
		public virtual Status Status { get; set; }
		public int ProjectId { get; set; }
		public virtual Project Project { get; set; }
		public string UserId { get; set; }
		public long TimeEstimate { get; set; }

		[ForeignKey("UserId")]
		public virtual ApplicationUser ApplicationUser { get; set; }
		public virtual ICollection<WorkLog> WorkLogs { get; set; }
		
	}
}
