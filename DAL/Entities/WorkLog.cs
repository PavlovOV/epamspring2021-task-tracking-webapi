﻿
namespace DAL.Entities
{
	public class WorkLog : BaseEntity
	{
		public string Description { get; set; }
		public long TimeLogged { get; set; }
		public int ObjectiveId { get; set; }
		public virtual Objective Objective { get; set; }
	}
}
