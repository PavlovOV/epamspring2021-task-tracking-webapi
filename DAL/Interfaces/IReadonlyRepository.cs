﻿using DAL.Entities;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
	public interface IReadonlyRepository<TEntity> where TEntity : BaseEntity
	{
	//	Task<Article> GetByIdWithDetailsAsync(int id);
		Task<TEntity> GetByIdAsync(int id);
		IQueryable<TEntity> FindAll();
	//	IQueryable<TEntity> FindAllWithDetails();
	}
}
