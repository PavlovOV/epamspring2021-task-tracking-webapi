﻿using DAL.Entities;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
	public interface IRepository<TEntity> where TEntity : BaseEntity
	{
		Task<TEntity> GetByIdWithDetailsAsync(int id);
		Task<TEntity> GetByIdAsync(int id);
		IQueryable<TEntity> FindAll();
		IQueryable<TEntity> FindAllWithDetails();
		void Add(TEntity entity);
		void Update(TEntity entity);
		
	}
}
