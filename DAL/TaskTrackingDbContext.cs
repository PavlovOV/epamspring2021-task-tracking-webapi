﻿using DAL.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;

//add-migration initial
//update-database
//Microsoft.EntityFrameworkCore.Tools
// 'WebAPI' need Microsoft.EntityFrameworkCore.Design
namespace DAL
{
	public class TaskTrackingDbContext : IdentityDbContext<ApplicationUser>
	{
		public DbSet<Project> Projects { get; set; }
		public DbSet<Objective> Objectives { get; set; }
		public DbSet<Status> Statuses { get; set; }
		public DbSet<WorkLog> WorkLogs { get; set; }
		public TaskTrackingDbContext(DbContextOptions<TaskTrackingDbContext> options)
			: base(options)
		{
		}

		private void SeedIdentity(ModelBuilder modelBuilder, string userManagerSanyaId, string userDeveloperDimaId,
			string userManagerPetroId, string userDeveloperVovaId)
		{
			modelBuilder.Entity<IdentityRole>().HasData(
				new IdentityRole[] {
									new IdentityRole
									{   Id = "2c5e174e-3b0e-446f-86af-483d56fd7210",
										Name = "Administrator",
										NormalizedName = "Administrator".ToUpper() },
									new IdentityRole
									{   Id = "2c5e174e-3b0e-446f-86af-483d56fd7211",
										Name = "Manager",
										NormalizedName = "Manager".ToUpper() },
									new IdentityRole
									{   Id = "2c5e174e-3b0e-446f-86af-483d56fd7212",
										Name = "Developer",
										NormalizedName = "Developer".ToUpper() }});

			var hasher = new PasswordHasher<ApplicationUser>();


			modelBuilder.Entity<ApplicationUser>().HasData(
				new ApplicationUser[] {
				new ApplicationUser
				{
					Id = "8e445865-a24d-4543-a6c6-9443d048cdb0",
					UserName = "vasya",
					Email= "test@gmail.com",
					NormalizedUserName = "vasya".ToUpper(),
					PasswordHash = hasher.HashPassword(null, "Vvv11_")
				},
				new ApplicationUser
				{
					Id = userManagerSanyaId ,
					UserName = "sanya",
					Email= "pvlalek@gmail.com",
					NormalizedUserName = "sanya".ToUpper(),
					PasswordHash = hasher.HashPassword(null, "Sss11_")
				},
				new ApplicationUser
				{
					Id = userManagerPetroId,
					UserName = "petro",
					Email= "poksol503@gmail.com",
					NormalizedUserName = "petro".ToUpper(),
					PasswordHash = hasher.HashPassword(null, "Ppp11_")
				},
				new ApplicationUser
				{
					Id = userDeveloperVovaId,
					UserName = "vova",
					Email= "cheshirepav@gmail.com",
					NormalizedUserName = "vova".ToUpper(),
					PasswordHash = hasher.HashPassword(null, "Vvv11_")
				},
				new ApplicationUser
				{
					Id = userDeveloperDimaId,
					UserName = "dima",
					Email= "dima020a@gmail.com",
					NormalizedUserName = "dima".ToUpper(),
					PasswordHash = hasher.HashPassword(null, "Ddd11_")
				}
				}
			);

			modelBuilder.Entity<IdentityUserRole<string>>().HasData(
					new IdentityUserRole<string>[] {
					  new IdentityUserRole<string>
						  {
							  RoleId = "2c5e174e-3b0e-446f-86af-483d56fd7210",
							  UserId = "8e445865-a24d-4543-a6c6-9443d048cdb0"
						  },
					   new IdentityUserRole<string>
						  {
							  RoleId = "2c5e174e-3b0e-446f-86af-483d56fd7212",
							  UserId = "8e445865-a24d-4543-a6c6-9443d048cdb0"
						  },
					 new IdentityUserRole<string>
						  {
							  RoleId ="2c5e174e-3b0e-446f-86af-483d56fd7211",
							  UserId =  userManagerSanyaId
						  },
					 new IdentityUserRole<string>
						  {
							  RoleId ="2c5e174e-3b0e-446f-86af-483d56fd7211",
							  UserId =  userManagerPetroId
						  },
					  new IdentityUserRole<string>
						  {
							  RoleId = "2c5e174e-3b0e-446f-86af-483d56fd7212",
							  UserId = userDeveloperDimaId
						  },
					  new IdentityUserRole<string>
						  {
							  RoleId = "2c5e174e-3b0e-446f-86af-483d56fd7212",
							  UserId = userDeveloperVovaId
						  },
			});
		}

		private void SeedDomain(ModelBuilder modelBuilder, string userManagerSanyaId, string userDeveloperDimaId,
			 string userManagerPetroId, string userDeveloperVovaId)
		{
			modelBuilder.Entity<Project>().HasData(
				new Project[]{
					new Project
					{
						Id = 1,
						UserId =userManagerSanyaId,
						Name = "Final project",
						Description = @"Create task tracking system.Issuance of the task by the manager. Job-status according to a workflow. Percentage of completion.
Mail notifications to system clients."
					},
					new Project
					{
						Id = 2,
						UserId =userManagerSanyaId,
						Name = "Test1 project 2",
						Description = @"Some thing."
					},
					new Project
					{
						Id = 3,
						UserId =userManagerSanyaId,
						Name = "Test2 project",
						Description = @"Some thing 3."
					},
					new Project
					{
						Id = 4,
						UserId =userManagerPetroId,
						Name = "Some training",
						Description = @"Some thing 4."
					},
				});

			modelBuilder.Entity<Status>().HasData(new Status[] {
				new Status
				{
					Id = 1,
					Name = "TO DO",
				},
					new Status
				{
					Id = 2,
					Name = "IN PROGRESS",
				},
						new Status
				{
					Id = 3,
					Name = "IN REVIEW",
				},
							new Status
				{
					Id = 4,
					Name = "DONE",
				},
			});

			modelBuilder.Entity<Objective>().HasData(new Objective[] {
				new Objective
				{
					Id = 1,
					Name = "Home work 23",
					Description="Create ASP.NET MVC apllication library prototype.",
					StatusId=4,
					ProjectId=4,
					UserId =userDeveloperDimaId,
					TimeEstimate = (new TimeSpan(6, 20, 0)).Ticks

				},
					new Objective
				{
					Id = 2,
					Name = "Home work 24. Low",
					Description=@"Create ASP.NET MVC apllication library prototype.
Create entities for domain
					 Store data in List.",
					StatusId=4,
					ProjectId=4,
					UserId =userDeveloperDimaId,
					TimeEstimate = (new TimeSpan(4, 50, 0)).Ticks
				},
					new Objective
				{
					Id = 3,
					Name = "Home work 24. Middle",
					Description=@"Create ASP.NET MVC apllication library prototype.
Store data in BD.",
					StatusId=3,
					ProjectId=4,
					UserId =userDeveloperVovaId,
					TimeEstimate = (new TimeSpan(25, 20, 0)).Ticks
				},
					new Objective
				{
					Id = 4,
					Name = "Home work 24. Advanced",
					Description=@"Create ASP.NET MVC apllication library prototype.
Use 3 level architecture.",
					StatusId=1,
					ProjectId=4,
					TimeEstimate = (new TimeSpan(21, 20, 0)).Ticks,
				},
					new Objective
				{
					Id = 5,
					Name = "Home work 25. Low",
					Description=@"Create ASP.NET MVC apllication library prototype.Use Ninject. 
Show articles in preview and full size. ",
					StatusId=2,
					ProjectId=3,
					UserId =userDeveloperDimaId,
					TimeEstimate = (new TimeSpan(51, 30, 0)).Ticks
				},
			});



			modelBuilder.Entity<WorkLog>().HasData(new WorkLog[] {
				new WorkLog
				{
					Id =1,
					ObjectiveId = 1,
					Description = "Implemented headers",
					TimeLogged= (new TimeSpan(0, 30, 0)).Ticks
				},
					new WorkLog
				{
					Id =2,
					ObjectiveId = 1,
					Description = "Implemented menu and three empty pages",
					TimeLogged= (new TimeSpan(0, 40, 0)).Ticks
				},
					new WorkLog
				{
					Id =3,
					ObjectiveId = 1,
					Description = "Implemented Article page",
					TimeLogged= (new TimeSpan(0, 40, 0)).Ticks
				},
					new WorkLog
				{
					Id =4,
					ObjectiveId = 1,
					Description = "Implemented Report page with report list and form",
					TimeLogged= (new TimeSpan(1, 10, 0)).Ticks
				},
					new WorkLog
				{
					Id =5,
					ObjectiveId = 1,
					Description = "Implemented Questionary pages with form ",
					TimeLogged= (new TimeSpan(1, 30, 0)).Ticks
				},

					new WorkLog
				{
					Id =6,
					ObjectiveId = 2,
					Description = "Created arcticle, report and questionary entity in js module",
					TimeLogged= (new TimeSpan(0, 30, 0)).Ticks
				},
					new WorkLog
				{
					Id =7,
					ObjectiveId = 2,
					Description = "Created lists of entities, filled with data and shown on pages ",
					TimeLogged= (new TimeSpan(1, 50, 0)).Ticks
				},

					new WorkLog
				{
					Id =8,
					ObjectiveId = 3,
					Description = "Created for bd entities ",
					TimeLogged= (new TimeSpan(2, 10, 0)).Ticks
				},
					new WorkLog
				{
					Id =9,
					ObjectiveId = 3,
					Description = "Seeded bd, test connection ",
					TimeLogged= (new TimeSpan(1, 10, 0)).Ticks
				},
					new WorkLog
				{
					Id =10,
					ObjectiveId = 3,
					Description = "Refreshed pages into  razor view."+System.Environment.NewLine + "// todo Path data to client, tested  ",
					TimeLogged= (new TimeSpan(2, 50, 0)).Ticks
				},


			});


		}
		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);
			string userManagerSanyaId = "8e445865-a24d-4543-a6c6-9443d048cdb1";
			string userManagerPetroId = "8e445865-a24d-4543-a6c6-9443d048cdb3";

			string userDeveloperDimaId = "8e445865-a24d-4543-a6c6-9443d048cdb2";
			string userDeveloperVovaId = "8e445865-a24d-4543-a6c6-9443d048cdb4";
			SeedIdentity(modelBuilder, userManagerSanyaId, userDeveloperDimaId, userManagerPetroId, userDeveloperVovaId);
			SeedDomain(modelBuilder, userManagerSanyaId, userDeveloperDimaId, userManagerPetroId, userDeveloperVovaId);
		}
	}
}
