﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class addedProject : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Reports",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reports", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Reports_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7210",
                column: "ConcurrencyStamp",
                value: "ba3bd856-9d27-4fe4-a867-445fb4b680ca");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7211",
                column: "ConcurrencyStamp",
                value: "41ee4794-2b15-4682-904a-a839487eca1d");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7212",
                column: "ConcurrencyStamp",
                value: "9b7723ea-2ded-478a-85e6-d09a923196b8");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb0",
                columns: new[] { "ConcurrencyStamp", "Email", "PasswordHash", "SecurityStamp" },
                values: new object[] { "3d4aeeac-6c6c-4542-a78b-0bc828842b87", "test@gmail.com", "AQAAAAEAACcQAAAAEI59qv4A9U3BuG3F5ivSbfQZyfgrmZRXjMJNYQKAzHPYnBPFYWhnSlef9GNfQAC5wA==", "a86dc539-8854-4117-a0f7-82c9de3d9ed7" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb1",
                columns: new[] { "ConcurrencyStamp", "Email", "PasswordHash", "SecurityStamp" },
                values: new object[] { "b9e514c2-6ffa-4142-917e-07a6e0d89fb5", "pvlalek@gmail.com", "AQAAAAEAACcQAAAAEMaTSp9SHjXi2TZX3N732gWffOtixdnTDhPR71Uw98kFmUjre+ji59RYPz6To8yZyA==", "0494770c-2a68-4fae-acdc-7375fa02892d" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb2",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "cc148dc2-9b47-4676-949f-1cfc09d1269b", "AQAAAAEAACcQAAAAEJ4HyAlhsSSxLCVMlXQe+oOAZ6Eu4LPFyTknzEaiyQrsa/7z4A1JHmC6pROlvdJwIA==", "479378b5-758c-4f69-a223-0c98281bb760" });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { "8e445865-a24d-4543-a6c6-9443d048cdb3", 0, "47e48472-53f5-4131-81a5-c2e439748f83", "pvlalek@gmail.com", false, false, null, null, "PETRO", "AQAAAAEAACcQAAAAEHI0wAZzFmr+MmK80pZY7wO/1Bl3UeZhJQGIoVIXWbkJz9pc9qciU6LsPY++XtqxLQ==", null, false, "155e2dd2-c714-41d7-814f-14f06bb4ab5c", false, "petro" },
                    { "8e445865-a24d-4543-a6c6-9443d048cdb4", 0, "f0f7eb7f-1f15-457d-8264-2144183ca43d", null, false, false, null, null, "VOVA", "AQAAAAEAACcQAAAAENTU3YMtWgW7hQtOHvAOCIfH6gyuH2BlvM1VRStvm2WbxzigsilHhI+45AtdU5NBwg==", null, false, "dcaf19a0-6e3b-437e-8435-6db7f66d20ee", false, "vova" }
                });

            migrationBuilder.InsertData(
                table: "Reports",
                columns: new[] { "Id", "Description", "Name", "UserId" },
                values: new object[,]
                {
                    { 1, "Create task tracking system.Issuance of the task by the manager. Job-status according to a workflow. Percentage of completion.\r\nMail notifications to system clients.", "Final project", "8e445865-a24d-4543-a6c6-9443d048cdb1" },
                    { 2, "Some thing.", "Test1 project 2", "8e445865-a24d-4543-a6c6-9443d048cdb1" },
                    { 3, "Some thing 3.", "Test2 project", "8e445865-a24d-4543-a6c6-9443d048cdb1" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "2c5e174e-3b0e-446f-86af-483d56fd7211", "8e445865-a24d-4543-a6c6-9443d048cdb3" });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "2c5e174e-3b0e-446f-86af-483d56fd7212", "8e445865-a24d-4543-a6c6-9443d048cdb4" });

            migrationBuilder.InsertData(
                table: "Reports",
                columns: new[] { "Id", "Description", "Name", "UserId" },
                values: new object[] { 4, "Some thing 4.", "Some training", "8e445865-a24d-4543-a6c6-9443d048cdb3" });

            migrationBuilder.CreateIndex(
                name: "IX_Reports_UserId",
                table: "Reports",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Reports");

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "2c5e174e-3b0e-446f-86af-483d56fd7211", "8e445865-a24d-4543-a6c6-9443d048cdb3" });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "2c5e174e-3b0e-446f-86af-483d56fd7212", "8e445865-a24d-4543-a6c6-9443d048cdb4" });

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb3");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb4");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7210",
                column: "ConcurrencyStamp",
                value: "0cf2a28e-9772-4d35-a24b-89dae4feb8be");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7211",
                column: "ConcurrencyStamp",
                value: "ce50e60b-8616-4a0c-bd55-15006b0ab98c");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7212",
                column: "ConcurrencyStamp",
                value: "ec394e87-3175-4e37-977a-9cd63a117841");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb0",
                columns: new[] { "ConcurrencyStamp", "Email", "PasswordHash", "SecurityStamp" },
                values: new object[] { "68f054f4-be53-4672-939d-798cf5fda788", null, "AQAAAAEAACcQAAAAEMVC1YdySIyQ95A7+pMpu12bLq2UCjkjafDEEVI4FDDlsydka+cjNLvWq77KO20YqQ==", "e7de1c84-e3a7-41db-8134-d901409192cd" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb1",
                columns: new[] { "ConcurrencyStamp", "Email", "PasswordHash", "SecurityStamp" },
                values: new object[] { "4356fbfa-788f-420f-8336-3a5e1017a3c4", null, "AQAAAAEAACcQAAAAEBG61oIFvGWIYCGUS65Hudzhq19ufxsqLfwI32CD5NpQwjZM7tkysLIzgl8xuMhp2g==", "047eb779-fab7-4e6a-893f-d6cc0637ebff" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb2",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "1a4fe52d-1ca0-4c2e-8cb8-bff0145564af", "AQAAAAEAACcQAAAAEFH9j1v2kNa3UPi1IA7+GL6sLxod16ldbAx28FPrTCQHAhxCXgYav530FXbxoj2VgA==", "66a8fdd0-537f-4f71-acc3-604effb49caf" });
        }
    }
}
