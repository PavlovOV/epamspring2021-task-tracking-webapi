﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class rename_ObjectiveWorkLog_into_WorkLog : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ObjectiveWorkLogs");

            migrationBuilder.CreateTable(
                name: "WorkLogs",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TimeLogged = table.Column<long>(type: "bigint", nullable: false),
                    ObjectiveId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkLogs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WorkLogs_Objectives_ObjectiveId",
                        column: x => x.ObjectiveId,
                        principalTable: "Objectives",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7210",
                column: "ConcurrencyStamp",
                value: "0f50ed3f-1ea9-468a-9649-dd96f2afdc21");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7211",
                column: "ConcurrencyStamp",
                value: "aa45b668-137e-4485-aa0e-06b424f63e70");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7212",
                column: "ConcurrencyStamp",
                value: "3a75a49b-fd04-4b93-8d08-e9227174b5f2");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb0",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "53922119-69b1-483f-889c-6b2b5be9967b", "AQAAAAEAACcQAAAAEGupBiUh0IdJ/TeiweQQPi7lktptrt9JCCrUSIwyCzDP3QWmsYSv1jQ7PqN6Rs0Kkw==", "283f7821-0532-4afa-98d7-593d721e0d06" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb1",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "0bfaf2db-1913-47cc-9414-64752edcc803", "AQAAAAEAACcQAAAAEBE+oCzA12gsyAgXLRTHTUyxDyhKV+5gxvElo8pxD5hAtLY7/oR6CHuxC0GoSvfZBQ==", "f858c326-bfa6-4c39-b0f0-078184255fa7" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb2",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "b8030f47-21d0-4b0e-97dc-ccc22f5368ff", "AQAAAAEAACcQAAAAED2VXZVmTI3UpFjuBKIX6qF+Q99K6xrpahpDpRcGwQPTChXyn+KugHRk+n52mDTP5Q==", "1a94305c-2269-447b-b639-45c446e2f2ae" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb3",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "9a22e72c-22aa-4496-bd77-7a80b15bea46", "AQAAAAEAACcQAAAAEMOENLth+OZnk6Cbd/t2EbDlfLyvccyWKkU51SUPBY43UuASGXCy25dkvB3PrGR0jQ==", "024dd443-34ac-48eb-857a-6747258bb640" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb4",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "dd317f34-eba3-4c8e-b520-ff74c58d0458", "AQAAAAEAACcQAAAAEAF2iSuT1VF3k0dfNYoHP2yeMudAvayFihoFMpPZWsPPqdLN+SOMrFyDuIfoq8JO2A==", "0469cb9a-e7d7-4390-9e9b-a5d30cc48989" });

            migrationBuilder.InsertData(
                table: "WorkLogs",
                columns: new[] { "Id", "Description", "ObjectiveId", "TimeLogged" },
                values: new object[,]
                {
                    { 10, "Refreshed pages into  razor view.\r\n// todo Path data to client, tested  ", 3, 102000000000L },
                    { 9, "Seeded bd, test connection ", 3, 42000000000L },
                    { 7, "Created lists of entities, filled with data and shown on pages ", 2, 66000000000L },
                    { 6, "Created arcticle, report and questionary entity in js module", 2, 18000000000L },
                    { 5, "Implemented Questionary pages with form ", 1, 54000000000L },
                    { 4, "Implemented Report page with report list and form", 1, 42000000000L },
                    { 3, "Implemented Article page", 1, 24000000000L },
                    { 2, "Implemented menu and three empty pages", 1, 24000000000L },
                    { 8, "Created for bd entities ", 3, 78000000000L },
                    { 1, "Implemented headers", 1, 18000000000L }
                });

            migrationBuilder.CreateIndex(
                name: "IX_WorkLogs_ObjectiveId",
                table: "WorkLogs",
                column: "ObjectiveId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WorkLogs");

            migrationBuilder.CreateTable(
                name: "ObjectiveWorkLogs",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ObjectiveId = table.Column<int>(type: "int", nullable: false),
                    TimeLogged = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ObjectiveWorkLogs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ObjectiveWorkLogs_Objectives_ObjectiveId",
                        column: x => x.ObjectiveId,
                        principalTable: "Objectives",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7210",
                column: "ConcurrencyStamp",
                value: "f88bcf06-0a71-40d3-be4e-84c695723a20");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7211",
                column: "ConcurrencyStamp",
                value: "e8944411-7cee-4763-b86c-b207561c9d7d");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7212",
                column: "ConcurrencyStamp",
                value: "84e7b9ab-7e71-4d26-9c55-08526bca92c6");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb0",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "4c542710-1565-4e60-bf2c-7350f219c820", "AQAAAAEAACcQAAAAEJqWmwnL7QbShIo14LXoPuVQEKqLf3GGx7FhN5YYnS+vZumDeiKx3IavYuLK3wVP3Q==", "996a02f0-5507-4590-b9c5-52a89190854b" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb1",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "24dcbf8c-8264-4375-aa59-4c203f032e18", "AQAAAAEAACcQAAAAEIxizFxEKiFIhwUlet0OHm5R442K1orNTVdMVlOjFWjCVSYdwepyqQTHZzKzrEtMWA==", "82f95e0e-50e7-4b7f-8c8e-41cedf10e6ee" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb2",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "02e3aace-e89b-497c-b179-65a2b058d73a", "AQAAAAEAACcQAAAAEPVIK53wd+b3WgmiwyovjWkxvPRsNlMoMNQlUbUmT26Vd4Lgjf7aHeX5qI70q7U+Qw==", "d325620a-707a-436c-95d4-a71919aca8e7" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb3",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "44cd399f-01e6-41ec-a382-0bccbd29f848", "AQAAAAEAACcQAAAAEO7tAZbTVnEYtdURtBSXgHdi4VKcBAJCZqA7CGFyt9JcigEpqOo/RJpMvNBCT5N9Cg==", "51f22339-927d-455c-8781-0f1c8ded2f21" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb4",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "3503bdb6-a08b-4cd8-a790-20441227ca51", "AQAAAAEAACcQAAAAEKTxHZpdYp54e/LkiWRrlCJP+wN99lbpQJwt3OvMb1r13Qmt8cqCnGeLH7wO+Bc3Yg==", "d8bfffaf-5146-4cb1-8cc5-f14bb07bf83b" });

            migrationBuilder.InsertData(
                table: "ObjectiveWorkLogs",
                columns: new[] { "Id", "Description", "ObjectiveId", "TimeLogged" },
                values: new object[,]
                {
                    { 10, "Refreshed pages into  razor view.\r\n// todo Path data to client, tested  ", 3, 102000000000L },
                    { 9, "Seeded bd, test connection ", 3, 42000000000L },
                    { 7, "Created lists of entities, filled with data and shown on pages ", 2, 66000000000L },
                    { 6, "Created arcticle, report and questionary entity in js module", 2, 18000000000L },
                    { 5, "Implemented Questionary pages with form ", 1, 54000000000L },
                    { 4, "Implemented Report page with report list and form", 1, 42000000000L },
                    { 3, "Implemented Article page", 1, 24000000000L },
                    { 2, "Implemented menu and three empty pages", 1, 24000000000L },
                    { 8, "Created for bd entities ", 3, 78000000000L },
                    { 1, "Implemented headers", 1, 18000000000L }
                });

            migrationBuilder.CreateIndex(
                name: "IX_ObjectiveWorkLogs_ObjectiveId",
                table: "ObjectiveWorkLogs",
                column: "ObjectiveId");
        }
    }
}
