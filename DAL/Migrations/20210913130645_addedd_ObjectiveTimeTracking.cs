﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class addedd_ObjectiveTimeTracking : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ObjectiveTimesTracking",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TimeLogged = table.Column<long>(type: "bigint", nullable: false),
                    ObjectiveId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ObjectiveTimesTracking", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ObjectiveTimesTracking_Objectives_ObjectiveId",
                        column: x => x.ObjectiveId,
                        principalTable: "Objectives",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7210",
                column: "ConcurrencyStamp",
                value: "517fa91a-a082-4eb0-a816-1a9b1ff997b7");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7211",
                column: "ConcurrencyStamp",
                value: "0c53ae45-9a9b-4dd8-b6c5-beade95bcb62");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7212",
                column: "ConcurrencyStamp",
                value: "1006b183-2b1e-436d-bf23-11c4f6dd8430");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb0",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "0f7c2e06-afd3-4816-8763-e694fee7c1ce", "AQAAAAEAACcQAAAAEG49/2EqtZZovgiNI8e2kv3tittjCoiQmHJqG8rm6xILuo2cwySzxnHhZx989ywdfQ==", "a76b1eed-9d4d-44f7-975a-de733af5f19d" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb1",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "b485841b-c14b-4e3b-bbee-da1b0630aac6", "AQAAAAEAACcQAAAAEGwLsTI0QqvhrzwbTsrqw66thK5MZV7Wx7dNpBDKnobiHvRkUW6sGV/Wxt+vAiwnwA==", "9718b15b-6298-4c2c-b1a6-a5abbbc3be60" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb2",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "ba3f4f90-1617-47aa-9cb1-f55a2de104fb", "AQAAAAEAACcQAAAAEC/74jReQB/ME7ax/U6kEEfEUFDSX11xcCq7bwiI6B9cvlr/NLXPJbD27aof3puRCA==", "a457323e-0a52-414e-85aa-7aa953f4d2bf" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb3",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "71d87065-c676-406a-a1cf-f3e98f0d29bf", "AQAAAAEAACcQAAAAEBozDvditiAfDUuADU45MhtmIpqFZ6t2X6iSsw+aomxNfdcqiG2dOyd/uPS2HamD/A==", "cddea4f0-e9a6-47a1-baf9-761ad6be05e7" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb4",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "de9fd3b9-5394-468d-9525-8be5266cbac9", "AQAAAAEAACcQAAAAEMNhWFgfI9d3PkzyMg5IIXI377KI35+kgomMOcaK5DsnWfNsaRS8FccE+s88CBDy3w==", "d34d3455-aa60-4d69-bc02-4a7fc6ee2922" });

            migrationBuilder.InsertData(
                table: "ObjectiveTimesTracking",
                columns: new[] { "Id", "Description", "ObjectiveId", "TimeLogged" },
                values: new object[,]
                {
                    { 10, "Refreshed pages into  razor view.\r\n// todo Path data to client, tested  ", 3, 102000000000L },
                    { 9, "Seeded bd, test connection ", 3, 42000000000L },
                    { 7, "Created lists of entities, filled with data and shown on pages ", 2, 66000000000L },
                    { 6, "Created arcticle, report and questionary entity in js module", 2, 18000000000L },
                    { 5, "Implemented Questionary pages with form ", 1, 54000000000L },
                    { 4, "Implemented Report page with report list and form", 1, 42000000000L },
                    { 3, "Implemented Article page", 1, 24000000000L },
                    { 2, "Implemented menu and three empty pages", 1, 24000000000L },
                    { 8, "Created for bd entities ", 3, 78000000000L },
                    { 1, "Implemented headers", 1, 18000000000L }
                });

            migrationBuilder.CreateIndex(
                name: "IX_ObjectiveTimesTracking_ObjectiveId",
                table: "ObjectiveTimesTracking",
                column: "ObjectiveId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ObjectiveTimesTracking");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7210",
                column: "ConcurrencyStamp",
                value: "d3fe2be4-be1d-41c4-8ec2-b952d5dc9497");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7211",
                column: "ConcurrencyStamp",
                value: "6babda64-6127-4fbd-8826-bdac3f65a764");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7212",
                column: "ConcurrencyStamp",
                value: "1abf1dc8-adbc-4807-984e-da2fb95ee727");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb0",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "63a34b4f-ebff-420f-91ff-9603244ee0cf", "AQAAAAEAACcQAAAAEPwMkZwP+AxVJY9PXqHk0hJnTnDKxkNW1zH3fJQ1vSbHn8UJ8w+IETqOomjBcCIOoA==", "dbf5fbb3-fe75-4159-9a02-1f23b06241da" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb1",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "3b7f535b-2006-4bae-877e-0d4cbe5a0a70", "AQAAAAEAACcQAAAAEOHcPQvbLU1pYC2LW1sYaL4Ar4KXFXZlISDSjWvo5La2DdXw7FstVcC9GrCsdYqPEQ==", "c0ad252f-abc0-4a47-94ec-a4c4a4cde7f8" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb2",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "fc88e451-a303-4287-8ea6-c1ffa6735de0", "AQAAAAEAACcQAAAAEA4QzdDbZoM+T/RxHfLOdBZCwrYLSn2U+cPXGvwDqOAEHzXAYhKLhp5eME55J1Rwxw==", "8ff888ca-5caa-41b5-9d0b-1eaa1e87b918" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb3",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "19ff6b05-30b7-4096-aa50-206a3bb9d8aa", "AQAAAAEAACcQAAAAEBjjB2zNRB81Y2pL8/jTN1YiJUHIJzfdrz4l7sC6eqwhTsDnQeOGfB/hLhdmG/N5kQ==", "26ecee12-6081-46b6-bc91-ff3bea69d3a6" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb4",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "1028b9f6-3d04-4fe2-b878-0dda706645ba", "AQAAAAEAACcQAAAAEKc9nou8+PM9qctbBgtWaIOmx+sqhUcFP7HsizzZV72x+BvrORdPHksI/62deADhyw==", "ebb98275-c3d7-460c-813d-4004c66bee5b" });
        }
    }
}
