﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class initial_add_email : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7210",
                column: "ConcurrencyStamp",
                value: "78801800-5d55-4e79-8df4-e79bbcaf022d");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7211",
                column: "ConcurrencyStamp",
                value: "651374e3-e231-4d89-87ae-a54bad1483c0");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7212",
                column: "ConcurrencyStamp",
                value: "bc0b7247-9fe0-49ba-bc3a-aca9be80bbc6");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb0",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "de0f7e40-797a-43fc-b4d3-38bff9cf2d1d", "AQAAAAEAACcQAAAAEGeOawSOQfa6yMOpnG4BhQse0J/BBPpNNj0dold1zhpmVtQwUEwU++IJ2MTX0NBfdw==", "fd7a28f1-6791-4ccf-8bb0-af6526e56576" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb1",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "8641408c-42fa-403e-9cef-e0912d7f0852", "AQAAAAEAACcQAAAAED2xS/U73EKLBgGfKyYGqP8nlnMF8UEaUIMyWU46i8L3mnlI9DTdqz61L+m+15evqw==", "ff5ac14d-2971-4a38-b837-29cc10609346" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb2",
                columns: new[] { "ConcurrencyStamp", "Email", "PasswordHash", "SecurityStamp" },
                values: new object[] { "9efd9ca9-e8ad-475f-8392-1cb1406318f6", "dima020a@gmail.com", "AQAAAAEAACcQAAAAELwOz0tvX+EJrnt6cuLjWy0J7vvGg/bi9FiwdErJw0/Qm0kxbHCPImdC7wk4jbBKnA==", "8abeec84-3a24-44c0-97dd-3ae20b6bf698" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb3",
                columns: new[] { "ConcurrencyStamp", "Email", "PasswordHash", "SecurityStamp" },
                values: new object[] { "e14adb64-c8f3-4059-b1ce-7ffa3fde3273", "poksol503@gmail.com", "AQAAAAEAACcQAAAAENXOKXRLZZfQzt5GLj39oG6J3B6vAiUM5ONQaTjugnjQFS8qZ1g2PvH6fdH5XYEOkA==", "4dc2e865-5a6b-4b68-b8e5-547014b0af51" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb4",
                columns: new[] { "ConcurrencyStamp", "Email", "PasswordHash", "SecurityStamp" },
                values: new object[] { "2ed6f5f3-5998-4620-b208-3a445b7ccd1f", "cheshirepav@gmail.com", "AQAAAAEAACcQAAAAEPk0RQHojRAvRpZ+j0mjKuvKt4j2VNrvFWDiKVP5rl+KPBhUlC9sMFr2Rv6Pv0OJGA==", "85b17f2f-9dcf-4b6c-b788-0d4ab6228974" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7210",
                column: "ConcurrencyStamp",
                value: "ab69ece0-1425-4993-b07c-b25f468de221");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7211",
                column: "ConcurrencyStamp",
                value: "24dafc5d-2ed5-4183-8228-349d65462ae8");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7212",
                column: "ConcurrencyStamp",
                value: "99fd1c1d-6aef-4794-a4dc-8ab9ee04c6af");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb0",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "34569b4e-c3bb-4723-b40e-6d4538c036a0", "AQAAAAEAACcQAAAAEDlRFAZq0na6D8n3cUbiDXuMu4Mkw+POPSmhtcyx3E7tlmIEvjEiM4oPq05LQ8vryQ==", "1c3f5489-4215-40f3-b40d-27c8d4d8cd0f" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb1",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "e56e1c7b-83c3-4eea-a802-7624f748fc26", "AQAAAAEAACcQAAAAEG9j9RGfLOeNlbUGU4m7kc9NUBns/yCSKQeB7oTQzkoTmFe5mcMOa8rCnKT7DQcyHg==", "84f15f1a-fab6-406d-93ef-ad9b5dd707cc" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb2",
                columns: new[] { "ConcurrencyStamp", "Email", "PasswordHash", "SecurityStamp" },
                values: new object[] { "186649dc-56dd-46c5-86fc-f43a0e9d7799", null, "AQAAAAEAACcQAAAAEBXxA2Z4ac5AwiK0Px7eZDiS07SKUACvaLxPcIq8zkpixDvh589y9LGf4yGCnf8toQ==", "78e37812-756c-43a6-9083-843476fd9982" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb3",
                columns: new[] { "ConcurrencyStamp", "Email", "PasswordHash", "SecurityStamp" },
                values: new object[] { "0af1ccdc-962e-41a6-beda-30d6bd46ee9f", "pvlalek@gmail.com", "AQAAAAEAACcQAAAAEPWIf0fPoyiL2+ciFwkHRVnV9ZqoTMRUOM8zd8Arnw+y1PG/KuO3SR63DmGomKWdyw==", "0927711b-b7b5-49cb-b61a-9700db926406" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb4",
                columns: new[] { "ConcurrencyStamp", "Email", "PasswordHash", "SecurityStamp" },
                values: new object[] { "147867f2-733e-40da-935b-b9b0134ef1bf", null, "AQAAAAEAACcQAAAAEP0Ykd650IIjcweXDgxhhOBwYUXh52PomwkkVgk0LhXWf5CO9TZXUonV8erBPmcQig==", "ee37d037-668e-4241-913f-0a0086761a77" });
        }
    }
}
