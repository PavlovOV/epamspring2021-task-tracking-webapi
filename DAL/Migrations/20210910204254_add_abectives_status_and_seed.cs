﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class add_abectives_status_and_seed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Status",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Status", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Objective",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    StatusId = table.Column<int>(type: "int", nullable: false),
                    ProjectId = table.Column<int>(type: "int", nullable: false),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Objective", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Objective_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Objective_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Objective_Status_StatusId",
                        column: x => x.StatusId,
                        principalTable: "Status",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7210",
                column: "ConcurrencyStamp",
                value: "ea8119ed-1379-4762-b018-82f6728155a1");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7211",
                column: "ConcurrencyStamp",
                value: "89ea18c6-8827-4584-b584-eca86c1745b4");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7212",
                column: "ConcurrencyStamp",
                value: "f9e365a7-67e9-4c1f-afd0-61cbe39a6167");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb0",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "731409f5-72b6-476b-b824-a8c32649e314", "AQAAAAEAACcQAAAAEO+WoHxn1FszKmWu2IcM28Nh5bz5nJ290taQ4rzGWkCuklmzq0wbI1gILMs8eP2cUA==", "089488c4-a480-4fff-b340-4d3df7bff68b" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb1",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "6e2ddc8e-0dbe-4e3a-baad-4c1352c60ec7", "AQAAAAEAACcQAAAAEI2MWFil49lSWyETOsewfB2KFGQCeWaMQ50RDE4qoLPgO+Cowa6ihfmgLyGUzRLmwg==", "b2d7d8af-eebc-440e-8889-785c749e8355" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb2",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "e3e80cec-7dd4-4949-92eb-360f8ff04be5", "AQAAAAEAACcQAAAAEAHCzcVNEsOT37SYvXHgn7LPXOOZ35ODYycLWZRVD95T6R0qwgS1wF/anKiGL56GjA==", "ce43adf9-f2f3-443d-8fa4-b200d102b5e6" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb3",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "a8d6a7a8-4abf-45b4-907d-93e516fc2d32", "AQAAAAEAACcQAAAAEAEJR68jrx7VECucvco978XqgrSls3K8QqmXXly0+VoTXfWndwS8B1K6ItWQ2GGbsw==", "8a9cf348-4d7f-4e9c-9fc5-2ba7973aeef3" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb4",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "f9a2ac7d-d567-48a3-99bb-d052c000b694", "AQAAAAEAACcQAAAAEElvaTWpJCcbGOsRfOZo5Z9Go7/X3vt3z/7lz2I7tg8Uy7yeRBpX9+yNeya05sd/Ig==", "00dfdd65-f850-4218-9522-e26418571136" });

            migrationBuilder.InsertData(
                table: "Status",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "TO DO" },
                    { 2, "IN PROGRESS" },
                    { 3, "IN REVIEW" },
                    { 4, "DONE" }
                });

            migrationBuilder.InsertData(
                table: "Objective",
                columns: new[] { "Id", "Description", "Name", "ProjectId", "StatusId", "UserId" },
                values: new object[,]
                {
                    { 4, "Create ASP.NET MVC apllication library prototype.\r\nUse 3 level architecture.", "Home work 24. Advanced", 4, 1, null },
                    { 3, "Create ASP.NET MVC apllication library prototype.\r\nStore data in BD.", "Home work 24. Middle", 4, 2, "8e445865-a24d-4543-a6c6-9443d048cdb4" },
                    { 5, "Create ASP.NET MVC apllication library prototype.Use Ninject. \r\nShow articles in preview and full size. ", "Home work 25. Low", 3, 2, "8e445865-a24d-4543-a6c6-9443d048cdb2" },
                    { 1, "Create ASP.NET MVC apllication library prototype.", "Home work 23", 4, 4, "8e445865-a24d-4543-a6c6-9443d048cdb2" },
                    { 2, "Create ASP.NET MVC apllication library prototype.\r\nCreate entities for domain\r\n					 Store data in List.", "Home work 24. Low", 4, 4, "8e445865-a24d-4543-a6c6-9443d048cdb2" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Objective_ProjectId",
                table: "Objective",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Objective_StatusId",
                table: "Objective",
                column: "StatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Objective_UserId",
                table: "Objective",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Objective");

            migrationBuilder.DropTable(
                name: "Status");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7210",
                column: "ConcurrencyStamp",
                value: "d5ee2294-41ee-4398-b6c4-98308790e7a6");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7211",
                column: "ConcurrencyStamp",
                value: "4e2a7cea-bfdc-4c26-8a14-d4c0186176df");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7212",
                column: "ConcurrencyStamp",
                value: "bebe2ee5-241a-4711-9e97-b270e563f2f0");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb0",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "b05d06a6-e8b9-4c8f-9e76-430fbf830b47", "AQAAAAEAACcQAAAAEAlBz6+aWd/AzLoQK6K1MUWJ/sBr/1jXb589aK0vgE/nDnKuE/aPV7cLTUviG4qvAg==", "f5388f7e-2920-4f54-866f-daef09f1eb0f" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb1",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "0d10270b-1ac6-454f-a62d-afb81efbf038", "AQAAAAEAACcQAAAAEIKKAJi7Gww6+tp18rsfAKvr6/sUkL1UNv01skA8n0ppHeZZWe2c06mMA/Mi6LX6vg==", "cf58b4c3-0d8d-4723-9127-a63b371b0eca" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb2",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "3902eed0-e1f2-4cd2-bc4e-50de83c9469f", "AQAAAAEAACcQAAAAEOiZwEThVyJtAkWB1g28XCI9xQoEAh2RnaU0pGp0P2fAQ1jqc0xLdEYW21OurIEtMw==", "779fb21a-1426-40f0-93d9-43e2199b7930" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb3",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "b6c10ed1-43fb-45b9-9f23-3a2074172875", "AQAAAAEAACcQAAAAENbpwHCHzvxxD9YH6IygS1W/AGXaf0CZ7gzo+423NnVuDAL5N5PrKBmtv/SIapLMEw==", "f04192f0-bbab-430c-90f6-c0964f3d5e70" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb4",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "64509081-4f90-4428-9538-9b29bcf1ffd3", "AQAAAAEAACcQAAAAEAONcRNr+FSAbxpb78M0sl4ua5EUUVYfRHa8byVDO1jckto8+11JB/dG51vMpQ1AlQ==", "568eece8-e299-4082-acca-fc2d2f5cedef" });
        }
    }
}
