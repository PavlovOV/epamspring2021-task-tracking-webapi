﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class rename_ObjectiveTimeTracking_into_ObjectiveWorkLog : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7210",
                column: "ConcurrencyStamp",
                value: "61c4a755-e30c-4ab0-abbe-3599fe92a7f7");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7211",
                column: "ConcurrencyStamp",
                value: "a013725a-14c9-4070-bd44-38546a61155b");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7212",
                column: "ConcurrencyStamp",
                value: "65f57074-afa7-41cb-bbe4-c78a1039b8de");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb0",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "5c57fa5b-02bf-42f9-9929-43d6f6f419ac", "AQAAAAEAACcQAAAAEIBbRTvwFRobjZCIsxCTWB+Q8TDcJgb8PDcgKInJ1iuC3EEof8BF9L83eJukJDS9bg==", "731ccc96-f3ff-466e-a8e9-f10fb9771b56" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb1",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "a57b2471-f764-4474-960e-31cd833ee027", "AQAAAAEAACcQAAAAEPd4skqM+z9G7QQV34tWQlqBE4jeR5b69VpSbuxZ0OPlhG9sLQGzcGZJaNocBwTlXA==", "662e347b-d28f-41b6-a101-9ad8b0347359" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb2",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "69068688-86a9-41dd-b7c7-053c2a8f442c", "AQAAAAEAACcQAAAAEK4yUaVEoGcnCviFhucpa0AKBTkVkNlqzXroE1bdJH60Dnzn7T0uyX7+bAL7tCVrcQ==", "3242e41d-9e63-4c3c-9284-0e2d9aa67201" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb3",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "f07fb4f7-41d0-4e2a-8938-5d9dd6b43266", "AQAAAAEAACcQAAAAEAfqhPG3MKwijH3zaqjHLC/UXLVRV/ba3NAjlyYjX5ZCl6I+sqianmAvlslqnLHR9Q==", "889cf3af-24bc-4d14-a4d1-3955d4b57536" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb4",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "a51189ca-032f-429a-8e6d-f45dc2b6249b", "AQAAAAEAACcQAAAAELaBUcfqyevEG1a8yeksTcYAaeewSKDZl0amDYrxR5zXF7VccnELZ0bG9BAhtHls4Q==", "40f753f1-bf3f-4b9a-ae14-a8760b05f3b9" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7210",
                column: "ConcurrencyStamp",
                value: "517fa91a-a082-4eb0-a816-1a9b1ff997b7");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7211",
                column: "ConcurrencyStamp",
                value: "0c53ae45-9a9b-4dd8-b6c5-beade95bcb62");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7212",
                column: "ConcurrencyStamp",
                value: "1006b183-2b1e-436d-bf23-11c4f6dd8430");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb0",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "0f7c2e06-afd3-4816-8763-e694fee7c1ce", "AQAAAAEAACcQAAAAEG49/2EqtZZovgiNI8e2kv3tittjCoiQmHJqG8rm6xILuo2cwySzxnHhZx989ywdfQ==", "a76b1eed-9d4d-44f7-975a-de733af5f19d" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb1",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "b485841b-c14b-4e3b-bbee-da1b0630aac6", "AQAAAAEAACcQAAAAEGwLsTI0QqvhrzwbTsrqw66thK5MZV7Wx7dNpBDKnobiHvRkUW6sGV/Wxt+vAiwnwA==", "9718b15b-6298-4c2c-b1a6-a5abbbc3be60" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb2",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "ba3f4f90-1617-47aa-9cb1-f55a2de104fb", "AQAAAAEAACcQAAAAEC/74jReQB/ME7ax/U6kEEfEUFDSX11xcCq7bwiI6B9cvlr/NLXPJbD27aof3puRCA==", "a457323e-0a52-414e-85aa-7aa953f4d2bf" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb3",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "71d87065-c676-406a-a1cf-f3e98f0d29bf", "AQAAAAEAACcQAAAAEBozDvditiAfDUuADU45MhtmIpqFZ6t2X6iSsw+aomxNfdcqiG2dOyd/uPS2HamD/A==", "cddea4f0-e9a6-47a1-baf9-761ad6be05e7" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb4",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "de9fd3b9-5394-468d-9525-8be5266cbac9", "AQAAAAEAACcQAAAAEMNhWFgfI9d3PkzyMg5IIXI377KI35+kgomMOcaK5DsnWfNsaRS8FccE+s88CBDy3w==", "d34d3455-aa60-4d69-bc02-4a7fc6ee2922" });
        }
    }
}
