﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class initial_made_vasya_olso_developer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7210",
                column: "ConcurrencyStamp",
                value: "d5ee2294-41ee-4398-b6c4-98308790e7a6");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7211",
                column: "ConcurrencyStamp",
                value: "4e2a7cea-bfdc-4c26-8a14-d4c0186176df");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7212",
                column: "ConcurrencyStamp",
                value: "bebe2ee5-241a-4711-9e97-b270e563f2f0");

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "2c5e174e-3b0e-446f-86af-483d56fd7212", "8e445865-a24d-4543-a6c6-9443d048cdb0" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb0",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "b05d06a6-e8b9-4c8f-9e76-430fbf830b47", "AQAAAAEAACcQAAAAEAlBz6+aWd/AzLoQK6K1MUWJ/sBr/1jXb589aK0vgE/nDnKuE/aPV7cLTUviG4qvAg==", "f5388f7e-2920-4f54-866f-daef09f1eb0f" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb1",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "0d10270b-1ac6-454f-a62d-afb81efbf038", "AQAAAAEAACcQAAAAEIKKAJi7Gww6+tp18rsfAKvr6/sUkL1UNv01skA8n0ppHeZZWe2c06mMA/Mi6LX6vg==", "cf58b4c3-0d8d-4723-9127-a63b371b0eca" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb2",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "3902eed0-e1f2-4cd2-bc4e-50de83c9469f", "AQAAAAEAACcQAAAAEOiZwEThVyJtAkWB1g28XCI9xQoEAh2RnaU0pGp0P2fAQ1jqc0xLdEYW21OurIEtMw==", "779fb21a-1426-40f0-93d9-43e2199b7930" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb3",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "b6c10ed1-43fb-45b9-9f23-3a2074172875", "AQAAAAEAACcQAAAAENbpwHCHzvxxD9YH6IygS1W/AGXaf0CZ7gzo+423NnVuDAL5N5PrKBmtv/SIapLMEw==", "f04192f0-bbab-430c-90f6-c0964f3d5e70" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb4",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "64509081-4f90-4428-9538-9b29bcf1ffd3", "AQAAAAEAACcQAAAAEAONcRNr+FSAbxpb78M0sl4ua5EUUVYfRHa8byVDO1jckto8+11JB/dG51vMpQ1AlQ==", "568eece8-e299-4082-acca-fc2d2f5cedef" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "2c5e174e-3b0e-446f-86af-483d56fd7212", "8e445865-a24d-4543-a6c6-9443d048cdb0" });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7210",
                column: "ConcurrencyStamp",
                value: "78801800-5d55-4e79-8df4-e79bbcaf022d");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7211",
                column: "ConcurrencyStamp",
                value: "651374e3-e231-4d89-87ae-a54bad1483c0");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7212",
                column: "ConcurrencyStamp",
                value: "bc0b7247-9fe0-49ba-bc3a-aca9be80bbc6");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb0",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "de0f7e40-797a-43fc-b4d3-38bff9cf2d1d", "AQAAAAEAACcQAAAAEGeOawSOQfa6yMOpnG4BhQse0J/BBPpNNj0dold1zhpmVtQwUEwU++IJ2MTX0NBfdw==", "fd7a28f1-6791-4ccf-8bb0-af6526e56576" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb1",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "8641408c-42fa-403e-9cef-e0912d7f0852", "AQAAAAEAACcQAAAAED2xS/U73EKLBgGfKyYGqP8nlnMF8UEaUIMyWU46i8L3mnlI9DTdqz61L+m+15evqw==", "ff5ac14d-2971-4a38-b837-29cc10609346" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb2",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "9efd9ca9-e8ad-475f-8392-1cb1406318f6", "AQAAAAEAACcQAAAAELwOz0tvX+EJrnt6cuLjWy0J7vvGg/bi9FiwdErJw0/Qm0kxbHCPImdC7wk4jbBKnA==", "8abeec84-3a24-44c0-97dd-3ae20b6bf698" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb3",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "e14adb64-c8f3-4059-b1ce-7ffa3fde3273", "AQAAAAEAACcQAAAAENXOKXRLZZfQzt5GLj39oG6J3B6vAiUM5ONQaTjugnjQFS8qZ1g2PvH6fdH5XYEOkA==", "4dc2e865-5a6b-4b68-b8e5-547014b0af51" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb4",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "2ed6f5f3-5998-4620-b208-3a445b7ccd1f", "AQAAAAEAACcQAAAAEPk0RQHojRAvRpZ+j0mjKuvKt4j2VNrvFWDiKVP5rl+KPBhUlC9sMFr2Rv6Pv0OJGA==", "85b17f2f-9dcf-4b6c-b788-0d4ab6228974" });
        }
    }
}
