﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class addedProject_fix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Reports_AspNetUsers_UserId",
                table: "Reports");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Reports",
                table: "Reports");

            migrationBuilder.RenameTable(
                name: "Reports",
                newName: "Projects");

            migrationBuilder.RenameIndex(
                name: "IX_Reports_UserId",
                table: "Projects",
                newName: "IX_Projects_UserId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Projects",
                table: "Projects",
                column: "Id");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7210",
                column: "ConcurrencyStamp",
                value: "ab69ece0-1425-4993-b07c-b25f468de221");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7211",
                column: "ConcurrencyStamp",
                value: "24dafc5d-2ed5-4183-8228-349d65462ae8");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7212",
                column: "ConcurrencyStamp",
                value: "99fd1c1d-6aef-4794-a4dc-8ab9ee04c6af");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb0",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "34569b4e-c3bb-4723-b40e-6d4538c036a0", "AQAAAAEAACcQAAAAEDlRFAZq0na6D8n3cUbiDXuMu4Mkw+POPSmhtcyx3E7tlmIEvjEiM4oPq05LQ8vryQ==", "1c3f5489-4215-40f3-b40d-27c8d4d8cd0f" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb1",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "e56e1c7b-83c3-4eea-a802-7624f748fc26", "AQAAAAEAACcQAAAAEG9j9RGfLOeNlbUGU4m7kc9NUBns/yCSKQeB7oTQzkoTmFe5mcMOa8rCnKT7DQcyHg==", "84f15f1a-fab6-406d-93ef-ad9b5dd707cc" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb2",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "186649dc-56dd-46c5-86fc-f43a0e9d7799", "AQAAAAEAACcQAAAAEBXxA2Z4ac5AwiK0Px7eZDiS07SKUACvaLxPcIq8zkpixDvh589y9LGf4yGCnf8toQ==", "78e37812-756c-43a6-9083-843476fd9982" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb3",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "0af1ccdc-962e-41a6-beda-30d6bd46ee9f", "AQAAAAEAACcQAAAAEPWIf0fPoyiL2+ciFwkHRVnV9ZqoTMRUOM8zd8Arnw+y1PG/KuO3SR63DmGomKWdyw==", "0927711b-b7b5-49cb-b61a-9700db926406" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb4",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "147867f2-733e-40da-935b-b9b0134ef1bf", "AQAAAAEAACcQAAAAEP0Ykd650IIjcweXDgxhhOBwYUXh52PomwkkVgk0LhXWf5CO9TZXUonV8erBPmcQig==", "ee37d037-668e-4241-913f-0a0086761a77" });

            migrationBuilder.AddForeignKey(
                name: "FK_Projects_AspNetUsers_UserId",
                table: "Projects",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Projects_AspNetUsers_UserId",
                table: "Projects");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Projects",
                table: "Projects");

            migrationBuilder.RenameTable(
                name: "Projects",
                newName: "Reports");

            migrationBuilder.RenameIndex(
                name: "IX_Projects_UserId",
                table: "Reports",
                newName: "IX_Reports_UserId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Reports",
                table: "Reports",
                column: "Id");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7210",
                column: "ConcurrencyStamp",
                value: "ba3bd856-9d27-4fe4-a867-445fb4b680ca");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7211",
                column: "ConcurrencyStamp",
                value: "41ee4794-2b15-4682-904a-a839487eca1d");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7212",
                column: "ConcurrencyStamp",
                value: "9b7723ea-2ded-478a-85e6-d09a923196b8");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb0",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "3d4aeeac-6c6c-4542-a78b-0bc828842b87", "AQAAAAEAACcQAAAAEI59qv4A9U3BuG3F5ivSbfQZyfgrmZRXjMJNYQKAzHPYnBPFYWhnSlef9GNfQAC5wA==", "a86dc539-8854-4117-a0f7-82c9de3d9ed7" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb1",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "b9e514c2-6ffa-4142-917e-07a6e0d89fb5", "AQAAAAEAACcQAAAAEMaTSp9SHjXi2TZX3N732gWffOtixdnTDhPR71Uw98kFmUjre+ji59RYPz6To8yZyA==", "0494770c-2a68-4fae-acdc-7375fa02892d" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb2",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "cc148dc2-9b47-4676-949f-1cfc09d1269b", "AQAAAAEAACcQAAAAEJ4HyAlhsSSxLCVMlXQe+oOAZ6Eu4LPFyTknzEaiyQrsa/7z4A1JHmC6pROlvdJwIA==", "479378b5-758c-4f69-a223-0c98281bb760" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb3",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "47e48472-53f5-4131-81a5-c2e439748f83", "AQAAAAEAACcQAAAAEHI0wAZzFmr+MmK80pZY7wO/1Bl3UeZhJQGIoVIXWbkJz9pc9qciU6LsPY++XtqxLQ==", "155e2dd2-c714-41d7-814f-14f06bb4ab5c" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb4",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "f0f7eb7f-1f15-457d-8264-2144183ca43d", "AQAAAAEAACcQAAAAENTU3YMtWgW7hQtOHvAOCIfH6gyuH2BlvM1VRStvm2WbxzigsilHhI+45AtdU5NBwg==", "dcaf19a0-6e3b-437e-8435-6db7f66d20ee" });

            migrationBuilder.AddForeignKey(
                name: "FK_Reports_AspNetUsers_UserId",
                table: "Reports",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
