﻿// <auto-generated />
using System;
using DAL;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace DAL.Migrations
{
    [DbContext(typeof(TaskTrackingDbContext))]
    [Migration("20210913134147_rename_ObjectiveTimeTracking_into_ObjectiveWorkLog_2")]
    partial class rename_ObjectiveTimeTracking_into_ObjectiveWorkLog_2
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.9")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("DAL.Entities.ApplicationUser", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("nvarchar(450)");

                    b.Property<int>("AccessFailedCount")
                        .HasColumnType("int");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Email")
                        .HasMaxLength(256)
                        .HasColumnType("nvarchar(256)");

                    b.Property<bool>("EmailConfirmed")
                        .HasColumnType("bit");

                    b.Property<bool>("LockoutEnabled")
                        .HasColumnType("bit");

                    b.Property<DateTimeOffset?>("LockoutEnd")
                        .HasColumnType("datetimeoffset");

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256)
                        .HasColumnType("nvarchar(256)");

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256)
                        .HasColumnType("nvarchar(256)");

                    b.Property<string>("PasswordHash")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("PhoneNumber")
                        .HasColumnType("nvarchar(max)");

                    b.Property<bool>("PhoneNumberConfirmed")
                        .HasColumnType("bit");

                    b.Property<string>("SecurityStamp")
                        .HasColumnType("nvarchar(max)");

                    b.Property<bool>("TwoFactorEnabled")
                        .HasColumnType("bit");

                    b.Property<string>("UserName")
                        .HasMaxLength(256)
                        .HasColumnType("nvarchar(256)");

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasDatabaseName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasDatabaseName("UserNameIndex")
                        .HasFilter("[NormalizedUserName] IS NOT NULL");

                    b.ToTable("AspNetUsers");

                    b.HasData(
                        new
                        {
                            Id = "8e445865-a24d-4543-a6c6-9443d048cdb0",
                            AccessFailedCount = 0,
                            ConcurrencyStamp = "4c542710-1565-4e60-bf2c-7350f219c820",
                            Email = "test@gmail.com",
                            EmailConfirmed = false,
                            LockoutEnabled = false,
                            NormalizedUserName = "VASYA",
                            PasswordHash = "AQAAAAEAACcQAAAAEJqWmwnL7QbShIo14LXoPuVQEKqLf3GGx7FhN5YYnS+vZumDeiKx3IavYuLK3wVP3Q==",
                            PhoneNumberConfirmed = false,
                            SecurityStamp = "996a02f0-5507-4590-b9c5-52a89190854b",
                            TwoFactorEnabled = false,
                            UserName = "vasya"
                        },
                        new
                        {
                            Id = "8e445865-a24d-4543-a6c6-9443d048cdb1",
                            AccessFailedCount = 0,
                            ConcurrencyStamp = "24dcbf8c-8264-4375-aa59-4c203f032e18",
                            Email = "pvlalek@gmail.com",
                            EmailConfirmed = false,
                            LockoutEnabled = false,
                            NormalizedUserName = "SANYA",
                            PasswordHash = "AQAAAAEAACcQAAAAEIxizFxEKiFIhwUlet0OHm5R442K1orNTVdMVlOjFWjCVSYdwepyqQTHZzKzrEtMWA==",
                            PhoneNumberConfirmed = false,
                            SecurityStamp = "82f95e0e-50e7-4b7f-8c8e-41cedf10e6ee",
                            TwoFactorEnabled = false,
                            UserName = "sanya"
                        },
                        new
                        {
                            Id = "8e445865-a24d-4543-a6c6-9443d048cdb3",
                            AccessFailedCount = 0,
                            ConcurrencyStamp = "44cd399f-01e6-41ec-a382-0bccbd29f848",
                            Email = "poksol503@gmail.com",
                            EmailConfirmed = false,
                            LockoutEnabled = false,
                            NormalizedUserName = "PETRO",
                            PasswordHash = "AQAAAAEAACcQAAAAEO7tAZbTVnEYtdURtBSXgHdi4VKcBAJCZqA7CGFyt9JcigEpqOo/RJpMvNBCT5N9Cg==",
                            PhoneNumberConfirmed = false,
                            SecurityStamp = "51f22339-927d-455c-8781-0f1c8ded2f21",
                            TwoFactorEnabled = false,
                            UserName = "petro"
                        },
                        new
                        {
                            Id = "8e445865-a24d-4543-a6c6-9443d048cdb4",
                            AccessFailedCount = 0,
                            ConcurrencyStamp = "3503bdb6-a08b-4cd8-a790-20441227ca51",
                            Email = "cheshirepav@gmail.com",
                            EmailConfirmed = false,
                            LockoutEnabled = false,
                            NormalizedUserName = "VOVA",
                            PasswordHash = "AQAAAAEAACcQAAAAEKTxHZpdYp54e/LkiWRrlCJP+wN99lbpQJwt3OvMb1r13Qmt8cqCnGeLH7wO+Bc3Yg==",
                            PhoneNumberConfirmed = false,
                            SecurityStamp = "d8bfffaf-5146-4cb1-8cc5-f14bb07bf83b",
                            TwoFactorEnabled = false,
                            UserName = "vova"
                        },
                        new
                        {
                            Id = "8e445865-a24d-4543-a6c6-9443d048cdb2",
                            AccessFailedCount = 0,
                            ConcurrencyStamp = "02e3aace-e89b-497c-b179-65a2b058d73a",
                            Email = "dima020a@gmail.com",
                            EmailConfirmed = false,
                            LockoutEnabled = false,
                            NormalizedUserName = "DIMA",
                            PasswordHash = "AQAAAAEAACcQAAAAEPVIK53wd+b3WgmiwyovjWkxvPRsNlMoMNQlUbUmT26Vd4Lgjf7aHeX5qI70q7U+Qw==",
                            PhoneNumberConfirmed = false,
                            SecurityStamp = "d325620a-707a-436c-95d4-a71919aca8e7",
                            TwoFactorEnabled = false,
                            UserName = "dima"
                        });
                });

            modelBuilder.Entity("DAL.Entities.Objective", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Description")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("ProjectId")
                        .HasColumnType("int");

                    b.Property<int>("StatusId")
                        .HasColumnType("int");

                    b.Property<long>("TimeEstimate")
                        .HasColumnType("bigint");

                    b.Property<string>("UserId")
                        .HasColumnType("nvarchar(450)");

                    b.HasKey("Id");

                    b.HasIndex("ProjectId");

                    b.HasIndex("StatusId");

                    b.HasIndex("UserId");

                    b.ToTable("Objectives");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Description = "Create ASP.NET MVC apllication library prototype.",
                            Name = "Home work 23",
                            ProjectId = 4,
                            StatusId = 4,
                            TimeEstimate = 228000000000L,
                            UserId = "8e445865-a24d-4543-a6c6-9443d048cdb2"
                        },
                        new
                        {
                            Id = 2,
                            Description = "Create ASP.NET MVC apllication library prototype.\r\nCreate entities for domain\r\n					 Store data in List.",
                            Name = "Home work 24. Low",
                            ProjectId = 4,
                            StatusId = 4,
                            TimeEstimate = 174000000000L,
                            UserId = "8e445865-a24d-4543-a6c6-9443d048cdb2"
                        },
                        new
                        {
                            Id = 3,
                            Description = "Create ASP.NET MVC apllication library prototype.\r\nStore data in BD.",
                            Name = "Home work 24. Middle",
                            ProjectId = 4,
                            StatusId = 2,
                            TimeEstimate = 912000000000L,
                            UserId = "8e445865-a24d-4543-a6c6-9443d048cdb4"
                        },
                        new
                        {
                            Id = 4,
                            Description = "Create ASP.NET MVC apllication library prototype.\r\nUse 3 level architecture.",
                            Name = "Home work 24. Advanced",
                            ProjectId = 4,
                            StatusId = 1,
                            TimeEstimate = 768000000000L
                        },
                        new
                        {
                            Id = 5,
                            Description = "Create ASP.NET MVC apllication library prototype.Use Ninject. \r\nShow articles in preview and full size. ",
                            Name = "Home work 25. Low",
                            ProjectId = 3,
                            StatusId = 2,
                            TimeEstimate = 1854000000000L,
                            UserId = "8e445865-a24d-4543-a6c6-9443d048cdb2"
                        });
                });

            modelBuilder.Entity("DAL.Entities.ObjectiveWorkLog", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Description")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("ObjectiveId")
                        .HasColumnType("int");

                    b.Property<long>("TimeLogged")
                        .HasColumnType("bigint");

                    b.HasKey("Id");

                    b.HasIndex("ObjectiveId");

                    b.ToTable("ObjectiveWorkLogs");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Description = "Implemented headers",
                            ObjectiveId = 1,
                            TimeLogged = 18000000000L
                        },
                        new
                        {
                            Id = 2,
                            Description = "Implemented menu and three empty pages",
                            ObjectiveId = 1,
                            TimeLogged = 24000000000L
                        },
                        new
                        {
                            Id = 3,
                            Description = "Implemented Article page",
                            ObjectiveId = 1,
                            TimeLogged = 24000000000L
                        },
                        new
                        {
                            Id = 4,
                            Description = "Implemented Report page with report list and form",
                            ObjectiveId = 1,
                            TimeLogged = 42000000000L
                        },
                        new
                        {
                            Id = 5,
                            Description = "Implemented Questionary pages with form ",
                            ObjectiveId = 1,
                            TimeLogged = 54000000000L
                        },
                        new
                        {
                            Id = 6,
                            Description = "Created arcticle, report and questionary entity in js module",
                            ObjectiveId = 2,
                            TimeLogged = 18000000000L
                        },
                        new
                        {
                            Id = 7,
                            Description = "Created lists of entities, filled with data and shown on pages ",
                            ObjectiveId = 2,
                            TimeLogged = 66000000000L
                        },
                        new
                        {
                            Id = 8,
                            Description = "Created for bd entities ",
                            ObjectiveId = 3,
                            TimeLogged = 78000000000L
                        },
                        new
                        {
                            Id = 9,
                            Description = "Seeded bd, test connection ",
                            ObjectiveId = 3,
                            TimeLogged = 42000000000L
                        },
                        new
                        {
                            Id = 10,
                            Description = "Refreshed pages into  razor view.\r\n// todo Path data to client, tested  ",
                            ObjectiveId = 3,
                            TimeLogged = 102000000000L
                        });
                });

            modelBuilder.Entity("DAL.Entities.Project", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Description")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("UserId")
                        .HasColumnType("nvarchar(450)");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("Projects");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Description = "Create task tracking system.Issuance of the task by the manager. Job-status according to a workflow. Percentage of completion.\r\nMail notifications to system clients.",
                            Name = "Final project",
                            UserId = "8e445865-a24d-4543-a6c6-9443d048cdb1"
                        },
                        new
                        {
                            Id = 2,
                            Description = "Some thing.",
                            Name = "Test1 project 2",
                            UserId = "8e445865-a24d-4543-a6c6-9443d048cdb1"
                        },
                        new
                        {
                            Id = 3,
                            Description = "Some thing 3.",
                            Name = "Test2 project",
                            UserId = "8e445865-a24d-4543-a6c6-9443d048cdb1"
                        },
                        new
                        {
                            Id = 4,
                            Description = "Some thing 4.",
                            Name = "Some training",
                            UserId = "8e445865-a24d-4543-a6c6-9443d048cdb3"
                        });
                });

            modelBuilder.Entity("DAL.Entities.Status", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Statuses");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Name = "TO DO"
                        },
                        new
                        {
                            Id = 2,
                            Name = "IN PROGRESS"
                        },
                        new
                        {
                            Id = 3,
                            Name = "IN REVIEW"
                        },
                        new
                        {
                            Id = 4,
                            Name = "DONE"
                        });
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRole", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .HasMaxLength(256)
                        .HasColumnType("nvarchar(256)");

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256)
                        .HasColumnType("nvarchar(256)");

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasDatabaseName("RoleNameIndex")
                        .HasFilter("[NormalizedName] IS NOT NULL");

                    b.ToTable("AspNetRoles");

                    b.HasData(
                        new
                        {
                            Id = "2c5e174e-3b0e-446f-86af-483d56fd7210",
                            ConcurrencyStamp = "f88bcf06-0a71-40d3-be4e-84c695723a20",
                            Name = "Administrator",
                            NormalizedName = "ADMINISTRATOR"
                        },
                        new
                        {
                            Id = "2c5e174e-3b0e-446f-86af-483d56fd7211",
                            ConcurrencyStamp = "e8944411-7cee-4763-b86c-b207561c9d7d",
                            Name = "Manager",
                            NormalizedName = "MANAGER"
                        },
                        new
                        {
                            Id = "2c5e174e-3b0e-446f-86af-483d56fd7212",
                            ConcurrencyStamp = "84e7b9ab-7e71-4d26-9c55-08526bca92c6",
                            Name = "Developer",
                            NormalizedName = "DEVELOPER"
                        });
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("ClaimType")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("ClaimValue")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("RoleId")
                        .IsRequired()
                        .HasColumnType("nvarchar(450)");

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("ClaimType")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("ClaimValue")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("UserId")
                        .IsRequired()
                        .HasColumnType("nvarchar(450)");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("ProviderKey")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("ProviderDisplayName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("UserId")
                        .IsRequired()
                        .HasColumnType("nvarchar(450)");

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("RoleId")
                        .HasColumnType("nvarchar(450)");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetUserRoles");

                    b.HasData(
                        new
                        {
                            UserId = "8e445865-a24d-4543-a6c6-9443d048cdb0",
                            RoleId = "2c5e174e-3b0e-446f-86af-483d56fd7210"
                        },
                        new
                        {
                            UserId = "8e445865-a24d-4543-a6c6-9443d048cdb0",
                            RoleId = "2c5e174e-3b0e-446f-86af-483d56fd7212"
                        },
                        new
                        {
                            UserId = "8e445865-a24d-4543-a6c6-9443d048cdb1",
                            RoleId = "2c5e174e-3b0e-446f-86af-483d56fd7211"
                        },
                        new
                        {
                            UserId = "8e445865-a24d-4543-a6c6-9443d048cdb3",
                            RoleId = "2c5e174e-3b0e-446f-86af-483d56fd7211"
                        },
                        new
                        {
                            UserId = "8e445865-a24d-4543-a6c6-9443d048cdb2",
                            RoleId = "2c5e174e-3b0e-446f-86af-483d56fd7212"
                        },
                        new
                        {
                            UserId = "8e445865-a24d-4543-a6c6-9443d048cdb4",
                            RoleId = "2c5e174e-3b0e-446f-86af-483d56fd7212"
                        });
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<string>", b =>
                {
                    b.Property<string>("UserId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("LoginProvider")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("Value")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("DAL.Entities.Objective", b =>
                {
                    b.HasOne("DAL.Entities.Project", "Project")
                        .WithMany()
                        .HasForeignKey("ProjectId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("DAL.Entities.Status", "Status")
                        .WithMany("Objectives")
                        .HasForeignKey("StatusId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("DAL.Entities.ApplicationUser", "ApplicationUser")
                        .WithMany("Objectives")
                        .HasForeignKey("UserId");

                    b.Navigation("ApplicationUser");

                    b.Navigation("Project");

                    b.Navigation("Status");
                });

            modelBuilder.Entity("DAL.Entities.ObjectiveWorkLog", b =>
                {
                    b.HasOne("DAL.Entities.Objective", "Objective")
                        .WithMany("ObjectiveWorkLogs")
                        .HasForeignKey("ObjectiveId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Objective");
                });

            modelBuilder.Entity("DAL.Entities.Project", b =>
                {
                    b.HasOne("DAL.Entities.ApplicationUser", "ApplicationUser")
                        .WithMany("Projects")
                        .HasForeignKey("UserId");

                    b.Navigation("ApplicationUser");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityRole", null)
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("DAL.Entities.ApplicationUser", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("DAL.Entities.ApplicationUser", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityRole", null)
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("DAL.Entities.ApplicationUser", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<string>", b =>
                {
                    b.HasOne("DAL.Entities.ApplicationUser", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("DAL.Entities.ApplicationUser", b =>
                {
                    b.Navigation("Objectives");

                    b.Navigation("Projects");
                });

            modelBuilder.Entity("DAL.Entities.Objective", b =>
                {
                    b.Navigation("ObjectiveWorkLogs");
                });

            modelBuilder.Entity("DAL.Entities.Status", b =>
                {
                    b.Navigation("Objectives");
                });
#pragma warning restore 612, 618
        }
    }
}
