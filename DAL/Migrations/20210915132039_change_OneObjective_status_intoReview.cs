﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class change_OneObjective_status_intoReview : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7210",
                column: "ConcurrencyStamp",
                value: "c1b38f8f-4347-44db-bf99-2ff0c2d62111");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7211",
                column: "ConcurrencyStamp",
                value: "edecac3c-45c7-4cd1-8061-3e0c58ecc468");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7212",
                column: "ConcurrencyStamp",
                value: "afe5df7c-987a-47ba-8119-8a15468e5242");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb0",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "8c50d232-505d-4af7-b964-1a6596cd7318", "AQAAAAEAACcQAAAAEDL3SoC3QfYpoXZN5oOuLoxfQYhq5SYrtJ/lIFMVl/PSIlCnQbHmc+NrNMamfJjeHg==", "6fea69bd-d86e-401d-9c0e-2e585325cc3a" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb1",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "d7d55134-40a5-4203-a136-6f8dccba61e3", "AQAAAAEAACcQAAAAEBMgxoMVtSIW57pNaX7RI2NjujW2fnYQtwm8VHs4zPdjg+frW/ZiIQQ58zqKoXjCIA==", "9cb3e2b3-8fcb-40c2-864b-f1c30872e866" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb2",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "e6f8e2b9-0651-4831-831b-3d134b20ca47", "AQAAAAEAACcQAAAAELdbGLT+Vg0ORAjjZIRBP88PWmNfoz3QYRA0v9tEb+eaJJ3vydtY1SlVNthDcW4hww==", "ec9c9502-d495-4b1e-bc4d-071bebc647d4" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb3",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "93c1d8a2-7f4a-43f6-965b-f782c8d6900e", "AQAAAAEAACcQAAAAEKWNhYKEQudle6pOuC0KbUOdPYdr9S2GbGTRWbjd7eecOqbx08HinhE6KOKtwe5ZTA==", "edbb7265-ccc6-4cda-b746-03a2f716fb0e" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb4",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "c0ece6d4-fcaa-4411-a3b0-d1ed21b64fa2", "AQAAAAEAACcQAAAAED/4Tl4r3uACgdvPHzZDS/rmuthmBUEszOIHszGnj26aE7zWRy6MQzCGfq2azgxEsQ==", "477b1501-5e93-4375-88ad-5b88140bfa52" });

            migrationBuilder.UpdateData(
                table: "Objectives",
                keyColumn: "Id",
                keyValue: 3,
                column: "StatusId",
                value: 3);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7210",
                column: "ConcurrencyStamp",
                value: "0f50ed3f-1ea9-468a-9649-dd96f2afdc21");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7211",
                column: "ConcurrencyStamp",
                value: "aa45b668-137e-4485-aa0e-06b424f63e70");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7212",
                column: "ConcurrencyStamp",
                value: "3a75a49b-fd04-4b93-8d08-e9227174b5f2");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb0",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "53922119-69b1-483f-889c-6b2b5be9967b", "AQAAAAEAACcQAAAAEGupBiUh0IdJ/TeiweQQPi7lktptrt9JCCrUSIwyCzDP3QWmsYSv1jQ7PqN6Rs0Kkw==", "283f7821-0532-4afa-98d7-593d721e0d06" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb1",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "0bfaf2db-1913-47cc-9414-64752edcc803", "AQAAAAEAACcQAAAAEBE+oCzA12gsyAgXLRTHTUyxDyhKV+5gxvElo8pxD5hAtLY7/oR6CHuxC0GoSvfZBQ==", "f858c326-bfa6-4c39-b0f0-078184255fa7" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb2",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "b8030f47-21d0-4b0e-97dc-ccc22f5368ff", "AQAAAAEAACcQAAAAED2VXZVmTI3UpFjuBKIX6qF+Q99K6xrpahpDpRcGwQPTChXyn+KugHRk+n52mDTP5Q==", "1a94305c-2269-447b-b639-45c446e2f2ae" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb3",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "9a22e72c-22aa-4496-bd77-7a80b15bea46", "AQAAAAEAACcQAAAAEMOENLth+OZnk6Cbd/t2EbDlfLyvccyWKkU51SUPBY43UuASGXCy25dkvB3PrGR0jQ==", "024dd443-34ac-48eb-857a-6747258bb640" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb4",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "dd317f34-eba3-4c8e-b520-ff74c58d0458", "AQAAAAEAACcQAAAAEAF2iSuT1VF3k0dfNYoHP2yeMudAvayFihoFMpPZWsPPqdLN+SOMrFyDuIfoq8JO2A==", "0469cb9a-e7d7-4390-9e9b-a5d30cc48989" });

            migrationBuilder.UpdateData(
                table: "Objectives",
                keyColumn: "Id",
                keyValue: 3,
                column: "StatusId",
                value: 2);
        }
    }
}
