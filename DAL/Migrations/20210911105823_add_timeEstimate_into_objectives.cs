﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class add_timeEstimate_into_objectives : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Objective_AspNetUsers_UserId",
                table: "Objective");

            migrationBuilder.DropForeignKey(
                name: "FK_Objective_Projects_ProjectId",
                table: "Objective");

            migrationBuilder.DropForeignKey(
                name: "FK_Objective_Status_StatusId",
                table: "Objective");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Status",
                table: "Status");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Objective",
                table: "Objective");

            migrationBuilder.RenameTable(
                name: "Status",
                newName: "Statuses");

            migrationBuilder.RenameTable(
                name: "Objective",
                newName: "Objectives");

            migrationBuilder.RenameIndex(
                name: "IX_Objective_UserId",
                table: "Objectives",
                newName: "IX_Objectives_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Objective_StatusId",
                table: "Objectives",
                newName: "IX_Objectives_StatusId");

            migrationBuilder.RenameIndex(
                name: "IX_Objective_ProjectId",
                table: "Objectives",
                newName: "IX_Objectives_ProjectId");

            migrationBuilder.AddColumn<long>(
                name: "TimeEstimate",
                table: "Objectives",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Statuses",
                table: "Statuses",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Objectives",
                table: "Objectives",
                column: "Id");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7210",
                column: "ConcurrencyStamp",
                value: "d3fe2be4-be1d-41c4-8ec2-b952d5dc9497");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7211",
                column: "ConcurrencyStamp",
                value: "6babda64-6127-4fbd-8826-bdac3f65a764");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7212",
                column: "ConcurrencyStamp",
                value: "1abf1dc8-adbc-4807-984e-da2fb95ee727");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb0",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "63a34b4f-ebff-420f-91ff-9603244ee0cf", "AQAAAAEAACcQAAAAEPwMkZwP+AxVJY9PXqHk0hJnTnDKxkNW1zH3fJQ1vSbHn8UJ8w+IETqOomjBcCIOoA==", "dbf5fbb3-fe75-4159-9a02-1f23b06241da" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb1",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "3b7f535b-2006-4bae-877e-0d4cbe5a0a70", "AQAAAAEAACcQAAAAEOHcPQvbLU1pYC2LW1sYaL4Ar4KXFXZlISDSjWvo5La2DdXw7FstVcC9GrCsdYqPEQ==", "c0ad252f-abc0-4a47-94ec-a4c4a4cde7f8" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb2",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "fc88e451-a303-4287-8ea6-c1ffa6735de0", "AQAAAAEAACcQAAAAEA4QzdDbZoM+T/RxHfLOdBZCwrYLSn2U+cPXGvwDqOAEHzXAYhKLhp5eME55J1Rwxw==", "8ff888ca-5caa-41b5-9d0b-1eaa1e87b918" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb3",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "19ff6b05-30b7-4096-aa50-206a3bb9d8aa", "AQAAAAEAACcQAAAAEBjjB2zNRB81Y2pL8/jTN1YiJUHIJzfdrz4l7sC6eqwhTsDnQeOGfB/hLhdmG/N5kQ==", "26ecee12-6081-46b6-bc91-ff3bea69d3a6" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb4",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "1028b9f6-3d04-4fe2-b878-0dda706645ba", "AQAAAAEAACcQAAAAEKc9nou8+PM9qctbBgtWaIOmx+sqhUcFP7HsizzZV72x+BvrORdPHksI/62deADhyw==", "ebb98275-c3d7-460c-813d-4004c66bee5b" });

            migrationBuilder.UpdateData(
                table: "Objectives",
                keyColumn: "Id",
                keyValue: 1,
                column: "TimeEstimate",
                value: 228000000000L);

            migrationBuilder.UpdateData(
                table: "Objectives",
                keyColumn: "Id",
                keyValue: 2,
                column: "TimeEstimate",
                value: 174000000000L);

            migrationBuilder.UpdateData(
                table: "Objectives",
                keyColumn: "Id",
                keyValue: 3,
                column: "TimeEstimate",
                value: 912000000000L);

            migrationBuilder.UpdateData(
                table: "Objectives",
                keyColumn: "Id",
                keyValue: 4,
                column: "TimeEstimate",
                value: 768000000000L);

            migrationBuilder.UpdateData(
                table: "Objectives",
                keyColumn: "Id",
                keyValue: 5,
                column: "TimeEstimate",
                value: 1854000000000L);

            migrationBuilder.AddForeignKey(
                name: "FK_Objectives_AspNetUsers_UserId",
                table: "Objectives",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Objectives_Projects_ProjectId",
                table: "Objectives",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Objectives_Statuses_StatusId",
                table: "Objectives",
                column: "StatusId",
                principalTable: "Statuses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Objectives_AspNetUsers_UserId",
                table: "Objectives");

            migrationBuilder.DropForeignKey(
                name: "FK_Objectives_Projects_ProjectId",
                table: "Objectives");

            migrationBuilder.DropForeignKey(
                name: "FK_Objectives_Statuses_StatusId",
                table: "Objectives");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Statuses",
                table: "Statuses");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Objectives",
                table: "Objectives");

            migrationBuilder.DropColumn(
                name: "TimeEstimate",
                table: "Objectives");

            migrationBuilder.RenameTable(
                name: "Statuses",
                newName: "Status");

            migrationBuilder.RenameTable(
                name: "Objectives",
                newName: "Objective");

            migrationBuilder.RenameIndex(
                name: "IX_Objectives_UserId",
                table: "Objective",
                newName: "IX_Objective_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Objectives_StatusId",
                table: "Objective",
                newName: "IX_Objective_StatusId");

            migrationBuilder.RenameIndex(
                name: "IX_Objectives_ProjectId",
                table: "Objective",
                newName: "IX_Objective_ProjectId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Status",
                table: "Status",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Objective",
                table: "Objective",
                column: "Id");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7210",
                column: "ConcurrencyStamp",
                value: "ea8119ed-1379-4762-b018-82f6728155a1");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7211",
                column: "ConcurrencyStamp",
                value: "89ea18c6-8827-4584-b584-eca86c1745b4");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7212",
                column: "ConcurrencyStamp",
                value: "f9e365a7-67e9-4c1f-afd0-61cbe39a6167");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb0",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "731409f5-72b6-476b-b824-a8c32649e314", "AQAAAAEAACcQAAAAEO+WoHxn1FszKmWu2IcM28Nh5bz5nJ290taQ4rzGWkCuklmzq0wbI1gILMs8eP2cUA==", "089488c4-a480-4fff-b340-4d3df7bff68b" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb1",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "6e2ddc8e-0dbe-4e3a-baad-4c1352c60ec7", "AQAAAAEAACcQAAAAEI2MWFil49lSWyETOsewfB2KFGQCeWaMQ50RDE4qoLPgO+Cowa6ihfmgLyGUzRLmwg==", "b2d7d8af-eebc-440e-8889-785c749e8355" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb2",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "e3e80cec-7dd4-4949-92eb-360f8ff04be5", "AQAAAAEAACcQAAAAEAHCzcVNEsOT37SYvXHgn7LPXOOZ35ODYycLWZRVD95T6R0qwgS1wF/anKiGL56GjA==", "ce43adf9-f2f3-443d-8fa4-b200d102b5e6" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb3",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "a8d6a7a8-4abf-45b4-907d-93e516fc2d32", "AQAAAAEAACcQAAAAEAEJR68jrx7VECucvco978XqgrSls3K8QqmXXly0+VoTXfWndwS8B1K6ItWQ2GGbsw==", "8a9cf348-4d7f-4e9c-9fc5-2ba7973aeef3" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb4",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "f9a2ac7d-d567-48a3-99bb-d052c000b694", "AQAAAAEAACcQAAAAEElvaTWpJCcbGOsRfOZo5Z9Go7/X3vt3z/7lz2I7tg8Uy7yeRBpX9+yNeya05sd/Ig==", "00dfdd65-f850-4218-9522-e26418571136" });

            migrationBuilder.AddForeignKey(
                name: "FK_Objective_AspNetUsers_UserId",
                table: "Objective",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Objective_Projects_ProjectId",
                table: "Objective",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Objective_Status_StatusId",
                table: "Objective",
                column: "StatusId",
                principalTable: "Status",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
