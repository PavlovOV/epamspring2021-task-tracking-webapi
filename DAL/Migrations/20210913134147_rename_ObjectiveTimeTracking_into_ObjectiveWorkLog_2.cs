﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class rename_ObjectiveTimeTracking_into_ObjectiveWorkLog_2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ObjectiveTimesTracking_Objectives_ObjectiveId",
                table: "ObjectiveTimesTracking");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ObjectiveTimesTracking",
                table: "ObjectiveTimesTracking");

            migrationBuilder.RenameTable(
                name: "ObjectiveTimesTracking",
                newName: "ObjectiveWorkLogs");

            migrationBuilder.RenameIndex(
                name: "IX_ObjectiveTimesTracking_ObjectiveId",
                table: "ObjectiveWorkLogs",
                newName: "IX_ObjectiveWorkLogs_ObjectiveId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ObjectiveWorkLogs",
                table: "ObjectiveWorkLogs",
                column: "Id");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7210",
                column: "ConcurrencyStamp",
                value: "f88bcf06-0a71-40d3-be4e-84c695723a20");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7211",
                column: "ConcurrencyStamp",
                value: "e8944411-7cee-4763-b86c-b207561c9d7d");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7212",
                column: "ConcurrencyStamp",
                value: "84e7b9ab-7e71-4d26-9c55-08526bca92c6");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb0",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "4c542710-1565-4e60-bf2c-7350f219c820", "AQAAAAEAACcQAAAAEJqWmwnL7QbShIo14LXoPuVQEKqLf3GGx7FhN5YYnS+vZumDeiKx3IavYuLK3wVP3Q==", "996a02f0-5507-4590-b9c5-52a89190854b" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb1",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "24dcbf8c-8264-4375-aa59-4c203f032e18", "AQAAAAEAACcQAAAAEIxizFxEKiFIhwUlet0OHm5R442K1orNTVdMVlOjFWjCVSYdwepyqQTHZzKzrEtMWA==", "82f95e0e-50e7-4b7f-8c8e-41cedf10e6ee" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb2",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "02e3aace-e89b-497c-b179-65a2b058d73a", "AQAAAAEAACcQAAAAEPVIK53wd+b3WgmiwyovjWkxvPRsNlMoMNQlUbUmT26Vd4Lgjf7aHeX5qI70q7U+Qw==", "d325620a-707a-436c-95d4-a71919aca8e7" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb3",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "44cd399f-01e6-41ec-a382-0bccbd29f848", "AQAAAAEAACcQAAAAEO7tAZbTVnEYtdURtBSXgHdi4VKcBAJCZqA7CGFyt9JcigEpqOo/RJpMvNBCT5N9Cg==", "51f22339-927d-455c-8781-0f1c8ded2f21" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb4",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "3503bdb6-a08b-4cd8-a790-20441227ca51", "AQAAAAEAACcQAAAAEKTxHZpdYp54e/LkiWRrlCJP+wN99lbpQJwt3OvMb1r13Qmt8cqCnGeLH7wO+Bc3Yg==", "d8bfffaf-5146-4cb1-8cc5-f14bb07bf83b" });

            migrationBuilder.AddForeignKey(
                name: "FK_ObjectiveWorkLogs_Objectives_ObjectiveId",
                table: "ObjectiveWorkLogs",
                column: "ObjectiveId",
                principalTable: "Objectives",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ObjectiveWorkLogs_Objectives_ObjectiveId",
                table: "ObjectiveWorkLogs");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ObjectiveWorkLogs",
                table: "ObjectiveWorkLogs");

            migrationBuilder.RenameTable(
                name: "ObjectiveWorkLogs",
                newName: "ObjectiveTimesTracking");

            migrationBuilder.RenameIndex(
                name: "IX_ObjectiveWorkLogs_ObjectiveId",
                table: "ObjectiveTimesTracking",
                newName: "IX_ObjectiveTimesTracking_ObjectiveId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ObjectiveTimesTracking",
                table: "ObjectiveTimesTracking",
                column: "Id");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7210",
                column: "ConcurrencyStamp",
                value: "61c4a755-e30c-4ab0-abbe-3599fe92a7f7");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7211",
                column: "ConcurrencyStamp",
                value: "a013725a-14c9-4070-bd44-38546a61155b");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7212",
                column: "ConcurrencyStamp",
                value: "65f57074-afa7-41cb-bbe4-c78a1039b8de");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb0",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "5c57fa5b-02bf-42f9-9929-43d6f6f419ac", "AQAAAAEAACcQAAAAEIBbRTvwFRobjZCIsxCTWB+Q8TDcJgb8PDcgKInJ1iuC3EEof8BF9L83eJukJDS9bg==", "731ccc96-f3ff-466e-a8e9-f10fb9771b56" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb1",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "a57b2471-f764-4474-960e-31cd833ee027", "AQAAAAEAACcQAAAAEPd4skqM+z9G7QQV34tWQlqBE4jeR5b69VpSbuxZ0OPlhG9sLQGzcGZJaNocBwTlXA==", "662e347b-d28f-41b6-a101-9ad8b0347359" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb2",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "69068688-86a9-41dd-b7c7-053c2a8f442c", "AQAAAAEAACcQAAAAEK4yUaVEoGcnCviFhucpa0AKBTkVkNlqzXroE1bdJH60Dnzn7T0uyX7+bAL7tCVrcQ==", "3242e41d-9e63-4c3c-9284-0e2d9aa67201" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb3",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "f07fb4f7-41d0-4e2a-8938-5d9dd6b43266", "AQAAAAEAACcQAAAAEAfqhPG3MKwijH3zaqjHLC/UXLVRV/ba3NAjlyYjX5ZCl6I+sqianmAvlslqnLHR9Q==", "889cf3af-24bc-4d14-a4d1-3955d4b57536" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb4",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "a51189ca-032f-429a-8e6d-f45dc2b6249b", "AQAAAAEAACcQAAAAELaBUcfqyevEG1a8yeksTcYAaeewSKDZl0amDYrxR5zXF7VccnELZ0bG9BAhtHls4Q==", "40f753f1-bf3f-4b9a-ae14-a8760b05f3b9" });

            migrationBuilder.AddForeignKey(
                name: "FK_ObjectiveTimesTracking_Objectives_ObjectiveId",
                table: "ObjectiveTimesTracking",
                column: "ObjectiveId",
                principalTable: "Objectives",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
