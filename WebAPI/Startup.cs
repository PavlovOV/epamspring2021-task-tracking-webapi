using BLL.Interfaces;
using BLL.Models.Mapper;
using BLL.Services;
using DAL;
using DAL.Entities;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using WebAPI.Filters;
using WebAPI.Models.Mapper;
using WebAPI.NotificationSettings;

namespace WebAPI
{
	public class Startup
	{
		private readonly string _contentRootPath;
		public IConfiguration Configuration { get; }
		public Startup(IConfiguration configuration, Microsoft.Extensions.Hosting.IHostingEnvironment env)
		{
			Configuration = configuration;
			_contentRootPath = env.ContentRootPath;
		}

		public void ConfigureServices(IServiceCollection services)
		{

			string connection = Configuration.GetConnectionString("DefaultConnection");
			if (connection.Contains("%CONTENTROOTPATH%"))
			{
				connection = connection.Replace("%CONTENTROOTPATH%", _contentRootPath);
			}

			services.AddDbContext<TaskTrackingDbContext>(options =>
				options.UseSqlServer(connection));

			services.AddIdentity<ApplicationUser, IdentityRole>()
		.AddEntityFrameworkStores<TaskTrackingDbContext>();

			services.AddAuthentication(opt =>
			{
				opt.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
				opt.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
			})
				.AddJwtBearer(options =>
				{
					options.TokenValidationParameters = new TokenValidationParameters
					{
						ValidateIssuer = true,
						ValidateAudience = true,
						ValidateLifetime = true,
						ValidateIssuerSigningKey = true,

						ValidIssuer = "http://localhost:4200",
						ValidAudience = "http://localhost:4200",
						IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("superSecret_superSecret_superSecret"))//todo  
					};
				});

			services.Configure<EmailDistributionInfo>(Configuration.GetSection("EmailDistributionInfo"));

			services.AddControllers(options =>
				options.Filters.Add(new DataExceptionAttribute()));

			services.AddScoped<IUnitOfWork, UnitOfWork>();

			services.AddTransient<IIdentityService, IdentityService>();
			services.AddTransient<IAuthenticationService, AuthenticationService>();
			services.AddTransient<INotificationService, NotificationService>();
			services.AddTransient<IStatusService, StatusService>();
			services.AddTransient<IProjectService,ProjectService>();
			services.AddTransient<IObjectiveService,ObjectiveService >();
			services.AddTransient<IWorkLogService, WorkLogService > ();



			services.AddSingleton<IMapperBLLCreator, MapperBLLCreator>();
			services.AddSingleton<IMapperViewCreator, MapperViewCreator>();

		}

		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app.UseHttpsRedirection();
			app.UseAuthentication();

			app.UseRouting();

			app.UseCors(builder => builder.AllowAnyOrigin()
			 .AllowAnyMethod().AllowAnyHeader());

			app.UseAuthorization();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllers();
			});

		}
	}
}
