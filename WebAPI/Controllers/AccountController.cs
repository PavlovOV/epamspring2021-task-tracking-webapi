﻿using AutoMapper;
using BLL.Interfaces;
using BLL.Models.Account;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WebAPI.Filters;
using WebAPI.Models.Identity;
using WebAPI.Models.Mapper;

namespace WebAPI.Controllers
{
	[Produces("application/json")]
	[Route("api/[controller]")]
	[DataExceptionAttribute]
	public class AccountController : ControllerBase
	{
		private readonly IMapper _mapper;
		private readonly IAuthenticationService _authenticationService;
		public AccountController( IMapperViewCreator _mapperCreator,	IAuthenticationService authenticationService)
		{
			_mapper = _mapperCreator.Mapper;
			_authenticationService = authenticationService;
		}

		[HttpPost, Route("login")]
		public async Task<IActionResult> Login([FromBody] UserLoginModel model)
		{
			if (ModelState.IsValid)
			{
				var userLoginDto = _mapper.Map<UserLoginDto>(model);
				var result = await _authenticationService.Login(userLoginDto);
				if (result.Succedeed)
				{
					var response = _mapper.Map<LoginResponseModel>(result.Response);
					return Ok(response);
				}
				else
				{
					foreach (var error in result.Errors)
						ModelState.AddModelError(string.Empty, error);
				}
			}
			return BadRequest(ModelState);
		}

		[HttpPut, Route("register")]
		public async Task<IActionResult> Register([FromBody] UserRegisterModel	model)
		{

			if (ModelState.IsValid)
			{
				var userRegisterDto = _mapper.Map<UserRegisterDto>(model);
				var result = await _authenticationService.Register(userRegisterDto);
				if (result.Succedeed)
				{
					var response = _mapper.Map<LoginResponseModel>(result.Response);
					return Ok(response);
				}
				else
				{
					foreach (var error in result.Errors)
						ModelState.AddModelError(string.Empty, error);
				}
			}
			return BadRequest(ModelState);
		}
	}
}
