﻿using AutoMapper;
using BLL.Interfaces;
using BLL.Models.Developer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using WebAPI.Filters;
using WebAPI.Models.Common;
using WebAPI.Models.Developer;
using WebAPI.Models.Mapper;

namespace WebAPI.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	[Authorize(Roles = "Developer")]
	[DataExceptionAttribute]
	public class DeveloperController : ControllerBase
	{
		private readonly IMapper _mapper;

		private readonly IProjectService _projectService;
		private readonly IObjectiveService _objectiveService;
		private readonly IWorkLogService _workLogService;

		public DeveloperController(IMapperViewCreator _mapperCreator, 
			 IProjectService projectService, IObjectiveService objectiveService,
			 IWorkLogService workLogService)
		{
			_mapper = _mapperCreator.Mapper;
			_projectService = projectService;
			_objectiveService = objectiveService;
			_workLogService = workLogService;
		}

		[HttpGet("projects/{developerId}")]
		public async Task<IEnumerable<ProjectModelView>> GetDeveloperProjects(string developerId)
		{
			var projects = await _projectService.GetProjectsForDeveloper(developerId);
			var result = _mapper.Map<IEnumerable<ProjectModelView>>(projects);
			return result;
		}

		[HttpGet("project/{projectId}")]
		public async Task<ProjectModelView> GetProject(int projectId)
		{
			var project = await _projectService.GetProject(projectId);
			var result = _mapper.Map<ProjectModelView>(project);
			return result;
		}

		[HttpGet("projects/tasks/{projectId}")]
		public async Task<IEnumerable<ObjectiveForDeveloperModelView>> GetAllObjectivseFromProject(int projectId)
		{
			ClaimsPrincipal claim = HttpContext.User;
			IEnumerable<ObjectiveForDeveloperDto> objectives = await _objectiveService.GetObjectivesForDeveloper(projectId, claim.Identity.Name);
			var result = _mapper.Map<IEnumerable<ObjectiveForDeveloperModelView>>(objectives);
			return result;
		}


		[HttpGet("projects/tasks/logworks/{objectiveId}")]
		public async Task<WorkLogsWithDetailsForDeveloperModelView> GetWorkLogsForObjective(int objectiveId)
		{
			var workLogs = await _workLogService.GenerateWorkLogsForDeveloperWithDetails("Developer", objectiveId);
			var result = _mapper.Map<WorkLogsWithDetailsForDeveloperModelView>(workLogs);
			return result;
		}
	
		[HttpPost("objective/{objectiveId}/status/{statusId}")]
		public async Task<IActionResult> ChangeObjectiveStatus(int objectiveId, int statusId)
		{
			await _objectiveService.ChangeObjectiveStatus("Developer", objectiveId, statusId);
			return Ok();
		}
		[HttpPut, Route("createworklog")]
		public async Task<IActionResult> CreateWorkLog(CreateWorkLogModel model)
		{
			if (ModelState.IsValid)
			{
				CreateWorkLogDto workLog = _mapper.Map<CreateWorkLogDto>(model);
				await _workLogService.CreateWorkLogAsync(workLog);
				return Ok();
			}
			return BadRequest(ModelState);
		}
	}
}
