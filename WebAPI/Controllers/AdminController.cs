﻿using AutoMapper;
using BLL.Interfaces;
using BLL.Models.Admin;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebAPI.Filters;
using WebAPI.Models.Admin;
using WebAPI.Models.Mapper;

namespace WebAPI.Controllers
{
	[Produces("application/json")]
	[Route("api/[controller]")]
	[Authorize(Roles = "Administrator")]
	[DataExceptionAttribute]
	public class AdminController : ControllerBase
	{
		private readonly IMapper _mapper;

		private readonly IIdentityService _identity;
		public AdminController( IMapperViewCreator _mapperCreator, IIdentityService identity)
		{
			_identity = identity;
			_mapper = _mapperCreator.Mapper;
		}


		[HttpGet, Route("roles")]
		public IEnumerable<RoleModelView> GetRoles()
		{
			var rolses = _identity.GetRoles();
			var result = _mapper.Map<IEnumerable<RoleModelView>>(rolses);
			return result;
		}

		[HttpPost, Route("users")]
		public async Task<UsersForAdminModelView> GetUsers([FromBody] DataForSearchFilterUsersForAdmin dataForFilterSort)
		{
			var filterSortDto = _mapper.Map<DataForUserSearchFilterDto>(dataForFilterSort);
			var complex = await _identity.GetUsersForPpecificRole(filterSortDto, dataForFilterSort.RoleId, dataForFilterSort.CurrentPage);
			var users = _mapper.Map<IEnumerable<UserForAdminModelView>>(complex.Item1);
			var result = new UsersForAdminModelView { Users = users, PagingInfo = complex.Item2 };
			return result;
		}
	}
}
