﻿using AutoMapper;
using BLL.Interfaces;
using BLL.Models.Manager;
using BLL.Models.Notification;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using WebAPI.Filters;
using WebAPI.Models.Common;
using WebAPI.Models.Manager;
using WebAPI.Models.Mapper;
using WebAPI.NotificationSettings;

namespace WebAPI.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	[Authorize(Roles = "Manager")]
	[DataExceptionAttribute]
	/// <summary> Class get email setting used for notification from  appsettings.json
	/// </summary>
	public class ManagerController : ControllerBase
	{
		private readonly IMapper _mapper;
		private readonly IProjectService _projectService;
		private readonly IObjectiveService _objectiveService;
		private readonly IWorkLogService _workLogService;


		private readonly IOptions<EmailDistributionInfo> _config;

		public ManagerController(IMapperViewCreator _mapperCreator, IProjectService projectService,
			IObjectiveService objectiveService,	 IWorkLogService workLogService,
			IOptions<EmailDistributionInfo> config)
		{
			_mapper = _mapperCreator.Mapper;
			_config = config;
			_projectService = projectService;
			_objectiveService = objectiveService;
			_workLogService = workLogService;
		}

		[HttpGet("projects/{managerId}")]
		public async Task<IEnumerable<ProjectModelView>> GetManagerProjects(string managerId)
		{
			var projects = await _projectService.GetProjectsForManager(managerId);
			var result = _mapper.Map<IEnumerable<ProjectModelView>>(projects);
			return result;
		}

		[HttpGet("project/{projectId}")]
		public async Task<ProjectModelView> GetProject(int projectId)
		{
			var project = await _projectService.GetProject(projectId);
			var result = _mapper.Map<ProjectModelView>(project);

			return result;
		}

		[HttpGet("projects/tasks/{projectId}")]
		public async Task<IEnumerable<ObjectiveForManagerModelView>> GetAllObjectivseFromProject(int projectId)
		{
			IEnumerable<ObjectiveForManagerDto> objectives = await _objectiveService.GetObjectivesForManager(projectId);
			var result = _mapper.Map<IEnumerable<ObjectiveForManagerModelView>>(objectives);
			return result;
		}

		[HttpPut, Route("createproject")]
		public async Task<IActionResult> CreateProject([FromBody] CreateProjectModel model)
		{
			if (ModelState.IsValid)
			{
				ClaimsPrincipal claim = HttpContext.User;
				var project = _mapper.Map<CreateProjectDto>(model);
				await _projectService.CreateProjectAsync(project, claim.Identity.Name);
				return Ok();
			}
			return BadRequest(ModelState);
		}

		[HttpGet("developers")]
		public async Task<IEnumerable<DeveloperModelView>> GetDevelopers()
		{
			var developers = await _objectiveService.GetDevelopers();
			var result = _mapper.Map<IEnumerable<DeveloperModelView>>(developers);
			return result;
		}

		/// <summary> Add new task into base. If new task has assisgned developer sent email notification.
		/// </summary>
		[HttpPut, Route("createobjective")]
		public async Task<IActionResult> CreateObjective(CreateObjectiveModel model)
		{
			if (ModelState.IsValid)
			{
				EmailDistributionInfo emailInfo = _config.Value;
				ClaimsPrincipal claim = HttpContext.User;
				CreateObjectiveDto objective = _mapper.Map<CreateObjectiveDto>(model);
				var emailInfoDto = _mapper.Map<EmailDistributionDto>(emailInfo);
				await _objectiveService.CreateObjectiveAsync(objective, claim.Identity.Name, emailInfoDto);
				return Ok();
			}
			return BadRequest(ModelState);
		}

		[HttpPost, Route("assigndeveloper")]
		public async Task<IActionResult> AssignDeveloper([FromBody] AssignDeveloperModel model)
		{
			EmailDistributionInfo emailInfo = _config.Value;
			ClaimsPrincipal claim = HttpContext.User;
			var assignDeveloper = _mapper.Map<AssignDeveloperDto>(model);
			var emailInfoDto = _mapper.Map<EmailDistributionDto>(emailInfo);
			await _objectiveService.AssignDeveloper(assignDeveloper, reporterName: claim.Identity.Name, emailInfoDto);
			return Ok();
		}

		[HttpPost("objective/{objectiveId}/status/{statusId}")]
		public async Task<IActionResult> ChangeObjectiveStatus(int objectiveId, int statusId)
		{
			await _objectiveService.ChangeObjectiveStatus("Manager", objectiveId, statusId);
			return Ok();
		}

		[HttpGet("projects/tasks/logworks/{objectiveId}")]
		public async Task<WorkLogsWithDetailsForManagerModelView> GetWorkLogsForObjective(int objectiveId)
		{
			var workLogs =await _workLogService.GenerateWorkLogsForManagerWithDetails("Manager", objectiveId);
			var result = _mapper.Map<WorkLogsWithDetailsForManagerModelView>(workLogs);
			return result;
		}
	}
}
