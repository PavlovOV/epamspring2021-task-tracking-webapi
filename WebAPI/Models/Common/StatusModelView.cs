﻿
namespace WebAPI.Models.Common
{
	public class StatusModelView
	{
		public int Id { get; set; }
		public string Name { get; set; }
	}
}
