﻿namespace WebAPI.Models.Identity
{
	public class UserModelView
	{
		public string Id { get; set; }
		public string Name { get; set; }
		public string Email { get; set; }

	}
}
