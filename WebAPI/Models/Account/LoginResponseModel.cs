﻿
namespace WebAPI.Models.Identity
{
	public class LoginResponseModel
	{
		public string Token { get; set; }
		public string UserName { get; set; }
		public string[] Roles { get; set; }
		public string UserId { get; set; }
	}
}
