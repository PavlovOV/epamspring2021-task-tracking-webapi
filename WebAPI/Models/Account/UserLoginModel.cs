﻿using System.ComponentModel.DataAnnotations;


namespace WebAPI.Models.Identity
{
	public class UserLoginModel
	{
		[Required]
		[StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 4)]
		public string Name { get; set; }

		[Required]
		[StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 4)]
		[DataType(DataType.Password)]
		public string Password { get; set; }

	}
}
