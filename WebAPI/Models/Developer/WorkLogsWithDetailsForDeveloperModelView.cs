﻿using System.Collections.Generic;
using WebAPI.Models.Common;
using WebAPI.Models.Manager;

namespace WebAPI.Models.Developer
{
	public class WorkLogsWithDetailsForDeveloperModelView
	{
		public ProjectModelView Project { get; set; }
		public ObjectiveForDeveloperModelView Objective { get; set; }
		public IEnumerable<WorkLogModelView> WorkLogs { get; set; }
		public IEnumerable<StatusModelView> PossibleStatuses { get; set; }
		public int Minutes { get; set; }
		public int Hours { get; set; }
		public int TotalDays { get; set; }
		public bool IsAllowedToAddLogWork { get; set; }
	}
}
