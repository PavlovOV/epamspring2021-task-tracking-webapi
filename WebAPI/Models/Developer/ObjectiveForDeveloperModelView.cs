﻿using WebAPI.Models.Common;

namespace WebAPI.Models.Developer
{
	public class ObjectiveForDeveloperModelView
	{
		public string Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public StatusModelView Status { get; set; }
		public int ProjectId { get; set; }

		public int Minutes { get; set; }
		public int Hours { get; set; }
		public int TotalDays { get; set; }
	}
}
