﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebAPI.Models.Developer
{
	public class CreateWorkLogModel
	{
		[Required]
		[StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 4)]
		public string Description { get; set; }

		[Required]
		public int ObjectiveId { get; set; }

		[Range(0, Int32.MaxValue, ErrorMessage = "Value should not be negative ")]
		public int Minutes { get; set; }
		[Range(0, Int32.MaxValue, ErrorMessage = "Value should not be negative ")]
		public int Hours { get; set; }
	}
}
