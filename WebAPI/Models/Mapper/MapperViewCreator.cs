﻿using AutoMapper;

namespace WebAPI.Models.Mapper
{
	public class MapperViewCreator : IMapperViewCreator
	{
		private readonly IMapper _mapper;
		public IMapper Mapper { get { return _mapper; } }
		public MapperViewCreator()
		{
			var myProfile = new AutomapperViewProfile();
			var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));

			_mapper = new AutoMapper.Mapper(configuration);
		}

	}
}
