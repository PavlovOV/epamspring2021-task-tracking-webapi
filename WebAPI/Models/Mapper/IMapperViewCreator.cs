﻿using AutoMapper;

namespace WebAPI.Models.Mapper
{
	public interface IMapperViewCreator
	{
		IMapper Mapper { get; }
	}
}
