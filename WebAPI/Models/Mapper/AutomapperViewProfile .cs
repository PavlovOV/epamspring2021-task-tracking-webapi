﻿using AutoMapper;
using BLL.Models;
using BLL.Models.Account;
using BLL.Models.Admin;
using BLL.Models.Common;
using BLL.Models.Developer;
using BLL.Models.Manager;
using BLL.Models.Notification;
using System;
using WebAPI.Models.Admin;
using WebAPI.Models.Common;
using WebAPI.Models.Developer;
using WebAPI.Models.Identity;
using WebAPI.Models.Manager;
using WebAPI.NotificationSettings;

namespace WebAPI.Models.Mapper
{
	class AutomapperViewProfile : Profile
	{
		public AutomapperViewProfile()
		{

			CreateMap<DataForSearchFilterUsersForAdmin, DataForUserSearchFilterDto>()
					.ForMember(c => c.Name, c => c.MapFrom(p => p.Name))
					.ForMember(c => c.NameOrder, c => c.MapFrom(p => p.NameOrder));

			CreateMap<UserForAdminDto, UserForAdminModelView>().ReverseMap();


			CreateMap<UserForAdminDto, UserForAdminModelView>().ReverseMap();


			CreateMap<RoleDto, RoleModelView>().ReverseMap();

			CreateMap<PagingInfo, PagingInfModeloView>()
				.ForMember(c => c.TotalPages, c => c.MapFrom(p => p.TotalPages));

			CreateMap<UserLoginModel, UserLoginDto>();
			CreateMap<LoginResponseDto, LoginResponseModel>();

			CreateMap<UserRegisterModel, UserRegisterDto>();

			// for manager
			CreateMap<ProjectDto, ProjectModelView>().ReverseMap();
			CreateMap<CreateProjectModel, CreateProjectDto>().ReverseMap();

			CreateMap<ObjectiveForManagerDto, ObjectiveForManagerModelView>()
				.ForMember(c => c.Minutes, c => c.MapFrom(p => p.TimeEstimate.Minutes))
				.ForMember(c => c.Hours, c => c.MapFrom(p => p.TimeEstimate.Hours))
				.ForMember(c => c.TotalDays, c => c.MapFrom(p => Math.Floor(p.TimeEstimate.TotalDays)));
			CreateMap<DeveloperDto, DeveloperModelView>();
			CreateMap<CreateObjectiveModel, CreateObjectiveDto>()
				.ForMember(c => c.TimeEstimate, c => c.MapFrom(p => new TimeSpan(p.Hours, p.Minutes, 0)));
			CreateMap<EmailDistributionInfo, EmailDistributionDto>();

			CreateMap<WorkLogDto, WorkLogModelView>()
				.ForMember(c => c.Minutes, c => c.MapFrom(p => p.TimeLogged.Minutes))
				.ForMember(c => c.Hours, c => c.MapFrom(p => p.TimeLogged.Hours))
				.ForMember(c => c.TotalDays, c => c.MapFrom(p => Math.Floor(p.TimeLogged.TotalDays)));

			CreateMap<StatusDto, StatusModelView>().ReverseMap();
			CreateMap<WorkLogsWithDetailsForManagerDto, WorkLogsWithDetailsForManagerModelView>()
				.ForMember(c => c.Minutes, c => c.MapFrom(p => p.TimeLogged.Minutes))
				.ForMember(c => c.Hours, c => c.MapFrom(p => p.TimeLogged.Hours))
				.ForMember(c => c.TotalDays, c => c.MapFrom(p => Math.Floor(p.TimeLogged.TotalDays)));

			CreateMap<AssignDeveloperModel, AssignDeveloperDto>();

			// for Developer
			CreateMap<ObjectiveForDeveloperDto, ObjectiveForDeveloperModelView>()
			.ForMember(c => c.Minutes, c => c.MapFrom(p => p.TimeEstimate.Minutes))
			.ForMember(c => c.Hours, c => c.MapFrom(p => p.TimeEstimate.Hours))
			.ForMember(c => c.TotalDays, c => c.MapFrom(p => Math.Floor(p.TimeEstimate.TotalDays)));

			CreateMap<WorkLogsWithDetailsForDeveloperDto, WorkLogsWithDetailsForDeveloperModelView>()
				.ForMember(c => c.Minutes, c => c.MapFrom(p => p.TimeLogged.Minutes))
				.ForMember(c => c.Hours, c => c.MapFrom(p => p.TimeLogged.Hours))
				.ForMember(c => c.TotalDays, c => c.MapFrom(p => Math.Floor(p.TimeLogged.TotalDays)));

			CreateMap<CreateWorkLogModel, CreateWorkLogDto>()
				.ForMember(c => c.TimeLogged, c => c.MapFrom(p => new TimeSpan(p.Hours, p.Minutes, 0)));
		}
	}
}
