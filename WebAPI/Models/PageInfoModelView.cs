﻿
namespace WebAPI.Models
{
	public class PagingInfModeloView
	{
		public int TotalItems { get; set; }
		public int ItemsPerPage { get; set; }
		public int CurrentPage { get; set; }
		public int TotalPages { get; set; }

	}
}
