﻿using WebAPI.Models.Common;

namespace WebAPI.Models.Manager
{
	public class ObjectiveForManagerModelView
	{
		public string Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public StatusModelView Status { get; set; }
		public int ProjectId { get; set; }
		public string UserId { get; set; }
		public string UserName { get; set; }
		public int Minutes { get; set; }
		public int Hours { get; set; }
		public int TotalDays { get; set; }
	}
}
