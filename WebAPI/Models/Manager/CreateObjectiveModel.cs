﻿using System;
using System.ComponentModel.DataAnnotations;


namespace WebAPI.Models.Manager
{
	public class CreateObjectiveModel
	{
		[Required]
		[StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 4)]
		public string Name { get; set; }

		[Required]
		[StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 4)]
		public string Description { get; set; }

		public int StatusId { get; set; }

		[Required]
		public int ProjectId { get; set; }

		public string UserId { get; set; }

		[Range(0, Int32.MaxValue, ErrorMessage = "Value should not be negative ")]
		public int Minutes { get; set; }
		[Range(0, Int32.MaxValue, ErrorMessage = "Value should not be negative ")]
		public int Hours { get; set; }
	}
}
