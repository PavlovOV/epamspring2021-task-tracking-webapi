﻿using System.Collections.Generic;
using WebAPI.Models.Common;

namespace WebAPI.Models.Manager
{
	public class WorkLogsWithDetailsForManagerModelView
	{
		public ProjectModelView Project { get; set; }
		public ObjectiveForManagerModelView Objective { get; set; }
		public IEnumerable<WorkLogModelView> WorkLogs { get; set; }
		public IEnumerable<StatusModelView> PossibleStatuses { get; set; }
		public int Minutes { get; set; }
		public int Hours { get; set; }
		public int TotalDays { get; set; }

	}
}
