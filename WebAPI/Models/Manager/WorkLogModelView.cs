﻿
namespace WebAPI.Models.Manager
{
	public class WorkLogModelView
	{
		public int Id { get; set; }
		public string Description { get; set; }
		public int Minutes { get; set; }
		public int Hours { get; set; }
		public int TotalDays { get; set; }
	}
}
