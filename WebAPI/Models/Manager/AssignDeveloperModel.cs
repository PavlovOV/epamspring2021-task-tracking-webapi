﻿
using System.ComponentModel.DataAnnotations;

namespace WebAPI.Models.Manager
{
	public class AssignDeveloperModel
	{
		[Required]
		public string DeveloperId { get; set; }
		[Required]
		public int ObjectiveId { get; set; }
	}
}
