﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebAPI.Models.Manager
{
	public class CreateProjectModel
	{
		[Required]
		[StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 4)]
		public string Name { get; set; }

		[Required]
		[StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 4)]
		public string Description { get; set; }

	}
}
