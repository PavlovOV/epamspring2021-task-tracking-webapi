﻿
namespace WebAPI.Models.Manager
{
	public class DeveloperModelView
	{
		public string Id { get; set; }
		public string Name { get; set; }
	}
}
