﻿
namespace WebAPI.Models.Admin
{
	public class RoleModelView
	{
		public string Id { get; set; }
		public string Name { get; set; }
	}
}
