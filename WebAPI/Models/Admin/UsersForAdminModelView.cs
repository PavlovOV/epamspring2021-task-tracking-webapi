﻿using BLL.Models;
using System.Collections.Generic;

namespace WebAPI.Models.Admin
{
	public class UsersForAdminModelView
	{
		public IEnumerable<UserForAdminModelView> Users { get; set; }
		public PagingInfo PagingInfo { get; set; }
	}
}
