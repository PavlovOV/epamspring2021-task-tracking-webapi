﻿using BLL.Models;

namespace WebAPI.Models.Admin
{
	public class DataForSearchFilterUsersForAdmin
	{
		public int CurrentPage { get; set; }
		public string Name { get; set; }
        public SortingOrder NameOrder { get; set; }
		public string RoleId { get; set; }


	}
}
