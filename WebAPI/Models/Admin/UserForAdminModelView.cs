﻿
namespace WebAPI.Models.Admin
{
	public class UserForAdminModelView
	{
		public string Id { get; set; }
		public string Name { get; set; }
		public string Email { get; set; }
	}
}
