﻿using BLL.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;

namespace WebAPI.Filters
{

	public class DataExceptionAttribute : Attribute, IExceptionFilter
	{
		public void OnException(ExceptionContext filterContext)
		{
			if (!filterContext.ExceptionHandled && filterContext.Exception is DataNotFoundException exception)
			{
				filterContext.Result = new ObjectResult(new string[]{ exception.Value })
				{
					StatusCode = exception.Status,
				};
				filterContext.ExceptionHandled = true;
			}
		}
	}

}
