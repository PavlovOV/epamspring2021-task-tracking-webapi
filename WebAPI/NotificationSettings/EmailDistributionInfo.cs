﻿namespace WebAPI.NotificationSettings
{
	public class EmailDistributionInfo
	{
		public string EmailAccount { get; set; }
		public string EmailPassword { get; set; }
		public string SMTP_HOST { get; set; }
		public string SMTP_PORT { get; set; }
	}
}
