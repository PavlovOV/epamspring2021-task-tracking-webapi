﻿using BLL.Interfaces;
using BLL.Models.Manager;
using BLL.Models.Mapper;
using BLL.Models.Notification;
using BLL.Services;
using DAL;
using DAL.Entities;
using Microsoft.AspNetCore.Identity;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TaskTracking.BLL.Test.BusinessTests
{
	public class ObjectiveServiceTests
	{
		readonly string userManagerSanyaId = "8e445865-a24d-4543-a6c6-9443d048cdb1";
		readonly string userManagerPetroId = "8e445865-a24d-4543-a6c6-9443d048cdb3";

		readonly string userDeveloperDimaId = "8e445865-a24d-4543-a6c6-9443d048cdb2";
		readonly string userDeveloperVovaId = "8e445865-a24d-4543-a6c6-9443d048cdb4";


		private IEnumerable<Project> GetTestProjects()
		{
			return new List<Project>()
			{
					new Project
					{
						Id = 1,
						UserId =userManagerSanyaId,
						Name = "Final project",
						Description = @"Create task tracking system.Issuance of the task by the manager. Job-status according to a workflow. Percentage of completion.
Mail notifications to system clients."
					},
					new Project
					{
						Id = 2,
						UserId =userManagerSanyaId,
						Name = "Test1 project 2",
						Description = @"Some thing."
					},
						new Project
					{
						Id = 3,
						UserId =userManagerSanyaId,
						Name = "Test2 project",
						Description = @"Some thing 3."
					},
						new Project
					{
						Id = 4,
						UserId =userManagerPetroId,
						Name = "Some training",
						Description = @"Some thing 4."
					},
						new Project
					{
						Id = 5,
						UserId =userManagerSanyaId,
						Name = "Some training",
						Description = @"Some thing 4."
					},
						new Project
					{
						Id = 6,
						UserId =userManagerSanyaId,
						Name = "Some training",
						Description = @"Some thing 4."
					},
					new Project
					{
						Id = 7,
						UserId =userManagerSanyaId,
						Name = "Some training",
						Description = @"Some thing 4."
					},
					new Project
					{
						Id = 8,
						UserId =userManagerSanyaId,
						Name = "Some training",
						Description = @"Some thing 4."
					},
					new Project
					{
						Id = 9,
						UserId =userManagerSanyaId,
						Name = "Some training",
						Description = @"Some thing 4."
					},
			};
		}

		private IEnumerable<ApplicationUser> GetTestApplicationUsers()
		{
			return new List<ApplicationUser>()
			{
				new ApplicationUser
				{
					Id = "8e445865-a24d-4543-a6c6-9443d048cdb0",
					UserName = "vasya",
					Email = "test@gmail.com",
					NormalizedUserName = "vasya".ToUpper(),

				},
				new ApplicationUser
				{
					Id = userManagerSanyaId,
					UserName = "sanya",
					Email = "pvlalek@gmail.com",
					NormalizedUserName = "sanya".ToUpper(),

				},
				new ApplicationUser
				{
					Id = userManagerPetroId,
					UserName = "petro",
					Email = "poksol503@gmail.com",
					NormalizedUserName = "petro".ToUpper(),

				},
				new ApplicationUser
				{
					Id = userDeveloperVovaId,
					UserName = "vova",
					Email = "cheshirepav@gmail.com",
					NormalizedUserName = "vova".ToUpper(),

				},
				new ApplicationUser
				{
					Id = userDeveloperDimaId,
					UserName = "dima",
					Email = "dima020a@gmail.com",
					NormalizedUserName = "dima".ToUpper(),

				}
			};
		}

		private IEnumerable<Objective> GetTestObjectivesWithDetails()
		{
			return new List<Objective>()

			{
				new Objective
				{
					Id = 1,
					Name = "Home work 23",
					Description = "Create ASP.NET MVC apllication library prototype.",
					StatusId = 4,
					ProjectId = 4,
					UserId = userDeveloperDimaId,
					TimeEstimate = (new TimeSpan(6, 20, 0)).Ticks,
					WorkLogs = new List<WorkLog>( GetTestWorkLoges().Where(x=>x.ObjectiveId==1) )

				},
				new Objective
				{
					Id = 2,
					Name = "Home work 24. Low",
					Description = @"Create ASP.NET MVC apllication library prototype.
Create entities for domain
					 Store data in List.",
					StatusId = 4,
					ProjectId = 4,
					UserId = userDeveloperDimaId,
					TimeEstimate = (new TimeSpan(4, 50, 0)).Ticks,
					WorkLogs = new List<WorkLog>( GetTestWorkLoges().Where(x=>x.ObjectiveId==2) )
				},
				new Objective
				{
					Id = 3,
					Name = "Home work 24. Middle",
					Description = @"Create ASP.NET MVC apllication library prototype.
Store data in BD.",
					StatusId = 3,
					ProjectId = 4,
					UserId = userDeveloperVovaId,
					TimeEstimate = (new TimeSpan(25, 20, 0)).Ticks,
					WorkLogs = new List<WorkLog>( GetTestWorkLoges().Where(x=>x.ObjectiveId==3) )
				},
				new Objective
				{
					Id = 4,
					Name = "Home work 24. Advanced",
					Description = @"Create ASP.NETure.",
					StatusId = 1,
					ProjectId = 4,
					TimeEstimate = (new TimeSpan(21, 20, 0)).Ticks,
					WorkLogs = new List<WorkLog>( GetTestWorkLoges().Where(x=>x.ObjectiveId==4) )
				},
				new Objective
				{
					Id = 5,
					Name = "Home work 25. Low",
					Description = @"Create ASP.NET MVC apllsize. ",
					StatusId = 2,
					ProjectId = 3,
					UserId = userDeveloperDimaId,
					TimeEstimate = (new TimeSpan(51, 30, 0)).Ticks,
					WorkLogs = new List<WorkLog>( GetTestWorkLoges().Where(x=>x.ObjectiveId==5) )
				},
				// project 2
					new Objective
				{
					Id = 6,
					Name = "Home work 25. Low",
					Description = @"Create ASP.NE size. ",
					StatusId = 1,
					ProjectId = 2,
					UserId = userDeveloperDimaId,
					TimeEstimate = (new TimeSpan(5, 30, 0)).Ticks,
					WorkLogs = new List<WorkLog>( GetTestWorkLoges().Where(x=>x.ObjectiveId==6) )
				},
						new Objective
				{
					Id = 7,
					Name = "Home work 25. Low",
					Description = @"Cre. ",
					StatusId = 1,
					ProjectId = 2,
					UserId = userDeveloperDimaId,
					TimeEstimate = (new TimeSpan(19, 30, 0)).Ticks,
					WorkLogs = new List<WorkLog>( GetTestWorkLoges().Where(x=>x.ObjectiveId==7) )
				},
// project 5
				new Objective
				{
					Id = 8,
					Name = "Home work 25. Low",
					Description = @"Create ASP.NE size. ",
					StatusId = 2,
					ProjectId = 5,
					UserId = userDeveloperDimaId,
					TimeEstimate = (new TimeSpan(19, 60, 0)).Ticks,
					WorkLogs = new List<WorkLog>( GetTestWorkLoges().Where(x=>x.ObjectiveId==8) )
				},
				new Objective
				{
					Id = 9,
					Name = "Home work 25. Low",
					Description = @"Cre. ",
					StatusId = 2,
					ProjectId = 6,
					UserId = userDeveloperDimaId,
					TimeEstimate = (new TimeSpan(6, 30, 0)).Ticks,
					WorkLogs = new List<WorkLog>( GetTestWorkLoges().Where(x=>x.ObjectiveId==9) )
				},
				// project 7
				new Objective
				{
					Id = 10,
					Name = "Home work 25. Low",
					Description = @"Cre. ",
					StatusId = 3,
					ProjectId = 7,
					UserId = userDeveloperDimaId,
					TimeEstimate = (new TimeSpan(6, 30, 0)).Ticks,
					WorkLogs = new List<WorkLog>( GetTestWorkLoges().Where(x=>x.ObjectiveId==10) )
				},
				// project 8
				new Objective
				{
					Id = 11,
					Name = "Home work 25. Low",
					Description = @"Cre. ",
					StatusId = 4,
					ProjectId = 8,
					UserId = userDeveloperDimaId,
					TimeEstimate = (new TimeSpan(6, 30, 0)).Ticks,
					WorkLogs = new List<WorkLog>( GetTestWorkLoges().Where(x=>x.ObjectiveId==11) )
				},
				// project 9
				new Objective
				{
					Id = 12,
					Name = "Home work 25. Low",
					Description = @"Cre. ",
					StatusId = 1,
					ProjectId = 9,
					UserId = userDeveloperDimaId,
					TimeEstimate = (new TimeSpan(8, 30, 0)).Ticks,
					WorkLogs = new List<WorkLog>( GetTestWorkLoges().Where(x=>x.ObjectiveId==12) )
				},
				new Objective
				{
					Id = 13,
					Name = "Home work 25. Low",
					Description = @"Cre. ",
					StatusId = 2,
					ProjectId = 9,
					UserId = userDeveloperDimaId,
					TimeEstimate = (new TimeSpan(24, 30, 0)).Ticks,
					WorkLogs = new List<WorkLog>( GetTestWorkLoges().Where(x=>x.ObjectiveId==13) )
				},
				new Objective
				{
					Id = 14,
					Name = "Home work 25. Low",
					Description = @"Cre. ",
					StatusId = 3,
					ProjectId = 9,
					UserId = userDeveloperDimaId,
					TimeEstimate = (new TimeSpan(5, 30, 0)).Ticks,
					WorkLogs = new List<WorkLog>( GetTestWorkLoges().Where(x=>x.ObjectiveId==14) )
				},
			};
		}

		private IEnumerable<WorkLog> GetTestWorkLoges()
		{
			return new List<WorkLog>()

			{
					new WorkLog
				{
					Id =1,
					ObjectiveId = 1,
					Description = "Implemented headers",
					TimeLogged= (new TimeSpan(0, 30, 0)).Ticks
				},
					new WorkLog
				{
					Id =2,
					ObjectiveId = 1,
					Description = "Implemented menu and three empty pages",
					TimeLogged= (new TimeSpan(0, 40, 0)).Ticks
				},
					new WorkLog
				{
					Id =3,
					ObjectiveId = 1,
					Description = "Implemented Article page",
					TimeLogged= (new TimeSpan(0, 40, 0)).Ticks
				},
					new WorkLog
				{
					Id =4,
					ObjectiveId = 1,
					Description = "Implemented Report page with report list and form",
					TimeLogged= (new TimeSpan(1, 10, 0)).Ticks
				},
					new WorkLog
				{
					Id =5,
					ObjectiveId = 1,
					Description = "Implemented Questionary pages with form ",
					TimeLogged= (new TimeSpan(1, 30, 0)).Ticks
				},

					new WorkLog
				{
					Id =6,
					ObjectiveId = 2,
					Description = "Created arcticle, report and questionary entity in js module",
					TimeLogged= (new TimeSpan(0, 30, 0)).Ticks
				},
					new WorkLog
				{
					Id =7,
					ObjectiveId = 2,
					Description = "Created lists of entities, filled with data and shown on pages ",
					TimeLogged= (new TimeSpan(1, 50, 0)).Ticks
				},
// 
					new WorkLog
				{
					Id =8,
					ObjectiveId = 3,
					Description = "Created for bd entities ",
					TimeLogged= (new TimeSpan(2, 10, 0)).Ticks
				},
					new WorkLog
				{
					Id =9,
					ObjectiveId = 3,
					Description = "Seeded bd, test connection ",
					TimeLogged= (new TimeSpan(1, 10, 0)).Ticks
				},
					new WorkLog
				{
					Id =10,
					ObjectiveId = 3,
					Description = "Refreshed pages into  razor view."+System.Environment.NewLine + "// todo Path data to client, tested  ",
					TimeLogged= (new TimeSpan(2, 50, 0)).Ticks
				},
					//project 5 Objective 8
					new WorkLog
				{
					Id =11,
					ObjectiveId = 8,
					Description = "Refresheient, tested  ",
					TimeLogged= (new TimeSpan(6, 30, 0)).Ticks
				},
					new WorkLog
				{
					Id =12,
					ObjectiveId = 8,
					Description = "Refreshed pagath data to client, tested  ",
					TimeLogged= (new TimeSpan(7, 30, 0)).Ticks
				},
						//project 6 Objective 9
					new WorkLog
				{
					Id =13,
					ObjectiveId = 9,
					Description = "Refresheient, tested  ",
					TimeLogged= (new TimeSpan(4, 30, 0)).Ticks
				},
					new WorkLog
				{
					Id =14,
					ObjectiveId = 10,
					Description = "Refreshed pagath data to client, tested  ",
					TimeLogged= (new TimeSpan(4, 30, 0)).Ticks
				},
				//project 7 Objective 10
					new WorkLog
				{
					Id =15,
					ObjectiveId = 9,
					Description = "Refresheient, tested  ",
					TimeLogged= (new TimeSpan(14, 30, 0)).Ticks
				},
					new WorkLog
				{
					Id =16,
					ObjectiveId = 10,
					Description = "Refreshed pagath data to client, tested  ",
					TimeLogged= (new TimeSpan(4, 30, 0)).Ticks
				},
					//project 8 Objective 11
					new WorkLog
				{
					Id =17,
					ObjectiveId = 11,
					Description = "Refresheient, tested  ",
					TimeLogged= (new TimeSpan(14, 30, 0)).Ticks
				},
					new WorkLog
				{
					Id =18,
					ObjectiveId = 11,
					Description = "Refreshed pagath data to client, tested  ",
					TimeLogged= (new TimeSpan(4, 30, 0)).Ticks
				},
					//project 9 Objective 13
					new WorkLog
				{
					Id =19,
					ObjectiveId = 13,
					Description = "Refresheient, tested  ",
					TimeLogged= (new TimeSpan(5, 30, 0)).Ticks
				},
					new WorkLog
				{
					Id =20,
					ObjectiveId = 13,
					Description = "Refreshed pagath data to client, tested  ",
					TimeLogged= (new TimeSpan(2, 30, 0)).Ticks
				},
					//project 9 Objective 14
				new WorkLog
				{
					Id =21,
					ObjectiveId = 14,
					Description = "Refresheient, tested  ",
					TimeLogged= (new TimeSpan(3, 30, 0)).Ticks
				},
					new WorkLog
				{
					Id =22,
					ObjectiveId = 14,
					Description = "Refreshed pagath data to client, tested  ",
					TimeLogged= (new TimeSpan(3, 30, 0)).Ticks
				},
			};
		}


		[Test]
		public async Task ObjectiveService_CalledNotificationWhenDeveloperAssigned()
		{

			foreach (var (userId, timesMethodCalled, failedMessage) in DataFor_Notification())
			{
				//arrange
				Mock<IUserStore<ApplicationUser>> UserStoreMock;
				UserStoreMock = new Mock<IUserStore<ApplicationUser>>();
				UserStoreMock.Setup(userManager => userManager.FindByIdAsync(It.IsAny<string>(), CancellationToken.None)).ReturnsAsync(
					(string id, CancellationToken c) =>
					{
						return null;
					});
				var mockUserManager = new Mock<UserManager<ApplicationUser>>(UserStoreMock.Object, null, null, null, null, null, null, null, null);

				mockUserManager.Setup(userManager => userManager.GetRolesAsync(It.IsAny<ApplicationUser>())).ReturnsAsync(
					(string id) =>
					{
						return new string[] { "Developer" };
					});

				var mockUnitOfWork = new Mock<IUnitOfWork>();
				mockUnitOfWork.Setup(m => m.Projects.GetByIdAsync(It.IsAny<int>()))
					.ReturnsAsync((int id) => { return GetTestProjects().FirstOrDefault(x => x.Id == id); });

				mockUnitOfWork.Setup(m => m.Objectives.FindAllWithDetails())
				.Returns(GetTestObjectivesWithDetails().AsQueryable);

				Mock<INotificationService> mockedNotificationService = new Mock<INotificationService>();

				IStatusService statusService = new StatusService(new MapperBLLCreator(), mockUnitOfWork.Object);
				IObjectiveService objectiveService = new ObjectiveService(new MapperBLLCreator(), mockUnitOfWork.Object, mockUserManager.Object,
				roleManager: null,notificationService: mockedNotificationService.Object, statusService: statusService);

				CreateObjectiveDto o = new CreateObjectiveDto() { UserId = userId };
				//act
				await objectiveService.CreateObjectiveAsync(o, "any name", null);
				//assert
				mockedNotificationService.Verify(
					m => m.EmailingAboutTaskAssign(It.IsAny<ObjectiveForManagerDto>(), It.IsAny<string>(), It.IsAny<EmailDistributionDto>()),
					timesMethodCalled,
					failedMessage);
			}
		}

		private IEnumerable<(string , Times methodCalled, string failedMessage)> DataFor_Notification()
		{
			return new List<(string, Times, string)>()
			{
				( "any id", Times.Once(), "test ManagerService: failed to call EmailingAboutTaskAssign in NotificationService when creating objective WITH assigned developer"),
				( null, Times.Never(), "test ManagerService: mistaken call EmailingAboutTaskAssign in NotificationService when creating objective WITHOUT assigned developer")
			};
		}

	}
}
