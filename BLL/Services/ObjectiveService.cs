﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using BLL.Exceptions;
using BLL.Interfaces;
using BLL.Models.Developer;
using BLL.Models.Manager;
using BLL.Models.Notification;
using BLL.Validation;
using DAL;
using DAL.Entities;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Services
{
	public class ObjectiveService: IObjectiveService
	{

		private readonly IMapper _mapper;
		private readonly IUnitOfWork _uow;
		private readonly UserManager<ApplicationUser> _userManager;
		private readonly RoleManager<IdentityRole> _roleManager;
		private readonly INotificationService _notificationService;
		private readonly IStatusService _statusService;


		public ObjectiveService(IMapperBLLCreator _mapperCreator, IUnitOfWork uow,
			UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager,
			INotificationService notificationService, IStatusService statusService)
		{
			_userManager = userManager;
			_roleManager = roleManager;
			_mapper = _mapperCreator.Mapper;
			_notificationService = notificationService;
			_uow = uow;
			_statusService = statusService;
		}

		public async Task CreateObjectiveAsync(CreateObjectiveDto objectiveDto, string reporterName, EmailDistributionDto emailInfo)
		{
			_statusService.SetInitialStatus(objectiveDto);
			Objective objective = _mapper.Map<Objective>(objectiveDto);
			if (string.IsNullOrEmpty(objective.UserId))
				objective.UserId = null;
			else
				if (!await UserAssigning.IsValid(_userManager, objective.UserId))
			{
				var user = await _userManager.FindByIdAsync(objective.UserId);
				throw new DataNotFoundException($"User {user.UserName} is not developer.")
				{ Status = 400, Value = "$User { user.UserName } is not developer." };
			}
			_uow.Objectives.Add(objective);
			objective.Id = 0;
			await _uow.SaveAsync();
			if (!string.IsNullOrEmpty(objective.UserId))
				await _notificationService.EmailingAboutTaskAssign(_mapper.Map<ObjectiveForManagerDto>(objective), reporterName, emailInfo);
		}

		public async Task ChangeObjectiveStatus(string role, int objectiveId, int newStatusId)
		{
			var objective = await _uow.Objectives.GetByIdAsync(objectiveId);
			if (objective == null)
				throw new DataNotFoundException("Task  not found.") { Status = 400, Value = "Task  not found." };
			var oldStatusId = objective.StatusId;

			if (await Validation.ChangeObjectiveStatus.IsValid(_statusService, role, oldStatusId, newStatusId))
			{
				objective.StatusId = newStatusId;
				_uow.Objectives.Update(objective);
				await _uow.SaveAsync();
			}
			else
			{
				string oldStatusName = (await _uow.Statuses.GetByIdAsync(oldStatusId)).Name;
				string newStatusName = (await _uow.Statuses.GetByIdAsync(newStatusId)).Name;
				throw new DataNotFoundException($"Cannot change status {oldStatusName} to {newStatusName}.");
			}
		}

		public async Task AssignDeveloper(AssignDeveloperDto assignDeveloperDto, string reporterName, EmailDistributionDto emailInfo)
		{
			if (!await UserAssigning.IsValid(_userManager, assignDeveloperDto.DeveloperId))
			{
				var user = await _userManager.FindByIdAsync(assignDeveloperDto.DeveloperId);
				throw new DataNotFoundException($"User {user.UserName} is not developer.") { Status = 400, Value = "$User { user.UserName } is not developer." };
			}
			var objective = await _uow.Objectives.GetByIdAsync(assignDeveloperDto.ObjectiveId);
			objective.UserId = assignDeveloperDto.DeveloperId;
			_uow.Objectives.Update(objective);
			await _uow.SaveAsync();
			await _notificationService.EmailingAboutTaskAssign(_mapper.Map<ObjectiveForManagerDto>(objective), reporterName, emailInfo);
		}
		public async Task<IEnumerable<ObjectiveForManagerDto>> GetObjectivesForManager(int projectId)
		{
			var project = await _uow.Projects.GetByIdAsync(projectId);
			if (project == null)
				throw new DataNotFoundException("Project not found.") { Status = 400, Value = "Project  not found." };
			var objectives = _uow.Objectives.FindAllWithDetails().Where(x => x.ProjectId == projectId);

			var result = _mapper.Map<IEnumerable<ObjectiveForManagerDto>>(objectives);

			return result;
		}

		public async Task<IEnumerable<ObjectiveForDeveloperDto>> GetObjectivesForDeveloper(int projectId,string name)
		{
			var project = await _uow.Projects.GetByIdAsync(projectId);
			if (project == null)

				throw new DataNotFoundException("Project not found.") { Status = 400, Value = "Project  not found." };
			var user = await _userManager.FindByNameAsync(name);
			if (user == null)
				throw new DataNotFoundException($"User {name} not found.") { Status = 404, Value = $"User {name} not found." };

			var objectives = _uow.Objectives.FindAllWithDetails().Where(x => x.ProjectId == projectId && x.UserId == user.Id);

			var result = _mapper.Map<IEnumerable<ObjectiveForDeveloperDto>>(objectives);

			return result;
		}

		public async Task<IEnumerable<DeveloperDto>> GetDevelopers()
		{
			var role = await _roleManager.FindByNameAsync("Developer");
			var users = await GetUsersByRole(role.Id);
			var result = _mapper.Map<IEnumerable<DeveloperDto>>(users);
			return result;
		}

		private async Task<IQueryable<DeveloperDto>> GetUsersByRole(string roleId)
		{
			var role = await _roleManager.FindByIdAsync(roleId);
			IQueryable<ApplicationUser> users;
			if (role == null)
				users = _userManager.Users;
			else
			{
				IQueryable<string> usersId = _uow.UserRoles.Where(x => x.RoleId == roleId).Select(x => x.UserId);
				users = from u in _userManager.Users
						join id in usersId on u.Id equals id
						select u;
			}
			return users.ProjectTo<DeveloperDto>(_mapper.ConfigurationProvider);
		}
	}
}
