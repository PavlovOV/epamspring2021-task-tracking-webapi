﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using BLL.Interfaces;
using BLL.Models;
using BLL.Models.Admin;
using DAL;
using DAL.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Services
{
	public class IdentityService : IIdentityService
	{
		private readonly IMapper _mapper;
		private readonly IUnitOfWork _uow;
		private readonly UserManager<ApplicationUser> _userManager;
		private readonly RoleManager<IdentityRole> _roleManager;

		public IdentityService(IMapperBLLCreator _mapperCreator, IUnitOfWork uow, UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
		{
			_mapper = _mapperCreator.Mapper;
			_uow = uow;
			_userManager = userManager;
			_roleManager = roleManager;
		}

		public IEnumerable<RoleDto> GetRoles()
		{
			var rolses = _roleManager.Roles;
			var result = _mapper.Map<IEnumerable<RoleDto>>(rolses);
			return result;
		}

		public async Task<(IEnumerable<UserForAdminDto>, PagingInfo)> GetUsersForPpecificRole(DataForUserSearchFilterDto filterSort, string roleId, int currentPage)
		{
			var usersByRole = await GetUsersByRole(roleId);
			IQueryable<UserForAdminDto> users = GetFilteredUsers(usersByRole, filterSort);

			PagingInfo pageInfo = new PagingInfo
			{
				CurrentPage = currentPage,
				ItemsPerPage = PagingInfo.PageSize,
				TotalItems = await users.CountAsync()
			};
			var page = await users.Skip((currentPage - 1) * PagingInfo.PageSize)
				.Take(PagingInfo.PageSize).ToListAsync();

			return (page, pageInfo);
		}

		private async Task<IQueryable<UserForAdminDto>> GetUsersByRole(string roleId)
		{
			var role = await _roleManager.FindByIdAsync(roleId);
			IQueryable<ApplicationUser> users;
			if (role == null)
				users = _userManager.Users;
			else
			{
				IQueryable<string> usersId = _uow.UserRoles.Where(x => x.RoleId == roleId).Select(x => x.UserId);
				users = from u in _userManager.Users
						join id in usersId on u.Id equals id
						select u;
			}
			return users.ProjectTo<UserForAdminDto>(_mapper.ConfigurationProvider);
		}

		private IQueryable<UserForAdminDto> GetFilteredUsers(IQueryable<UserForAdminDto> users, DataForUserSearchFilterDto filterSort)
		{
			var usersFiltered = users.Where(x => x.Name.Contains(filterSort.Name));
			var orderedUsers =
				(filterSort.NameOrder == SortingOrder.descending) ?
					usersFiltered.OrderByDescending(x => x.Name) : usersFiltered.OrderBy(x => x.Name);
			return orderedUsers;
		}

	}
}
