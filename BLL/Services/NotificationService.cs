﻿using BLL.Interfaces;
using BLL.Models.Manager;
using BLL.Models.Notification;
using DAL;
using DAL.Entities;
using Microsoft.AspNetCore.Identity;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace BLL.Services
{
	public class NotificationService : INotificationService
	{
		private readonly UserManager<ApplicationUser> _userManager;
		private readonly IUnitOfWork _uow;

		public NotificationService(IUnitOfWork uow, UserManager<ApplicationUser> userManager)
		{
			_userManager = userManager;
			_uow = uow;
		}
		public async Task EmailingAboutTaskAssign(ObjectiveForManagerDto objective, string reporterName, EmailDistributionDto emailInfo)
		{
			var reporter = await _userManager.FindByNameAsync(reporterName);
			var assigner = await _userManager.FindByIdAsync(objective.UserId);
			var project = await _uow.Projects.GetByIdAsync(objective.ProjectId);
			string body = $"Dear {assigner.UserName}."+ System.Environment.NewLine+
				$"We inform you that manager {reporter.UserName} assigned task {objective.Name} in project {project.Name}." +
				System.Environment.NewLine + 
				$"Kindest regard.";
			string subject = "Task assign";
			string assignerEmail = assigner.Email;
			await SendEmail(subject, body, emailInfo, assignerEmail);
		}

		private async Task SendEmail(string subject, string body, EmailDistributionDto emailInfo, string assignerEmail)
		{
			using (MailMessage mail = new MailMessage())
			{
				mail.From = new MailAddress(emailInfo.EmailAccount);
				mail.To.Add(assignerEmail);
				mail.Subject = subject;
				mail.Body = body;
				mail.IsBodyHtml = true;
				using (SmtpClient smtp = new SmtpClient(emailInfo.SMTP_HOST, int.Parse(emailInfo.SMTP_PORT)))
				{
					smtp.UseDefaultCredentials = true;
					smtp.Credentials = new NetworkCredential(emailInfo.EmailAccount, emailInfo.EmailPassword);
					smtp.EnableSsl = true;
					await smtp.SendMailAsync(mail);
				}
			}
		}
	}
}
