﻿using BLL.Interfaces;
using BLL.Models.Account;
using DAL.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
	public class AuthenticationService : IAuthenticationService
	{
		private readonly UserManager<ApplicationUser> _userManager;
		private readonly SignInManager<ApplicationUser> _signInManager;


		public AuthenticationService(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager)
		{
			_userManager = userManager;
			_signInManager = signInManager;
		}
		public async Task<AuthenticationResultDto> Login(UserLoginDto userModel)
		{
			var attempt = await _signInManager.PasswordSignInAsync(userModel.Name, userModel.Password, false, false);
			LoginResponseDto response = null;
			bool succedeed = false;
			if (attempt.Succeeded)
			{
				succedeed = true;
				var user = await _userManager.FindByNameAsync(userModel.Name);
				string[] roles = (await _userManager.GetRolesAsync(user)).ToArray();
				var tokenString = TokenCreate(userModel.Name, roles);

				response = new LoginResponseDto
				{
					Token = tokenString,
					UserName = userModel.Name,
					Roles = roles,
					UserId = user.Id
				};
			}
			AuthenticationResultDto result = new AuthenticationResultDto
			{
				Response = response,
				Succedeed = succedeed,
				Errors = new List<string> { "Invalid username or password." }
			};
			return result;
		}

		public async Task<AuthenticationResultDto> Register(UserRegisterDto model)
		{
			ApplicationUser user = new ApplicationUser
			{
				Email = model.Email,
				UserName = model.Name
			};
			LoginResponseDto response = null;
			bool succedeed = false;
			var errors = new List<string>();

			var resultCreate = await _userManager.CreateAsync(user, model.Password);
			if (resultCreate.Succeeded)
			{
				var resultAddToRole = await _userManager.AddToRoleAsync(user, "Developer");
				if (resultAddToRole.Succeeded)
				{
					succedeed = true;
					var tokenString = TokenCreate(model.Name, new string[] { "Developer" });
					string[] roles = new[] { "Developer" };
					response = new LoginResponseDto
					{
						Token = tokenString,
						UserName = model.Name,
						Roles = roles,
						UserId = user.Id
					};
				}
				else
				{
					foreach (var error in resultAddToRole.Errors)
						errors.Add(error.Description);
				}
			}
			else
			{
				foreach (var error in resultCreate.Errors)
					errors.Add(error.Description);
			}

			AuthenticationResultDto result = new AuthenticationResultDto
			{
				Response = response,
				Succedeed = succedeed,
				Errors = errors
			};
			return result;
		}

		private string TokenCreate(string name, IEnumerable<string> roles)
		{
			var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("superSecret_superSecret_superSecret"));
			var signingCreential = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);

			var claims = new List<Claim>();
			claims.Add(new Claim(ClaimTypes.Name, name));

			foreach (var role in roles)
				claims.Add(new Claim(ClaimTypes.Role, role));

			var jwt = new JwtSecurityToken(
			issuer: "http://localhost:4200",
			audience: "http://localhost:4200",
			claims: claims,//identity.Claims,
			expires: DateTime.Now.AddMinutes(25),
			signingCredentials: signingCreential
			);

			var tokenString = new JwtSecurityTokenHandler().WriteToken(jwt);
			return tokenString;
		}
	}
}

