﻿using AutoMapper;
using BLL.Interfaces;
using BLL.Models.Common;
using BLL.Models.Manager;
using DAL;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Services
{
	public class StatusService: IStatusService
	{

		private readonly IMapper _mapper;
		private readonly IUnitOfWork _uow;

		public StatusService(IMapperBLLCreator _mapperCreator, IUnitOfWork uow)
		{
			_mapper = _mapperCreator.Mapper;
			_uow = uow;
		}
		public	 async Task<IEnumerable<StatusDto>> GeneratePossibleStatuses(string role, StatusDesignation  status)
		{
			var statusesId = GeneratePossibleStatusesId(role, status);
			List<StatusDto> statuses = new List<StatusDto>();
			foreach(var id in statusesId)
				statuses.Add(_mapper.Map<StatusDto>(await _uow.Statuses.GetByIdAsync(id)));

			return statuses;
		}
		public void SetInitialStatus(CreateObjectiveDto objective)
		{
			objective.StatusId = (int)StatusDesignation.ToDo;
		}
		private IEnumerable<int> GeneratePossibleStatusesId(string role, StatusDesignation status)
		{
			if (role.Equals("Manager"))
			{
				switch (status)
				{
					case StatusDesignation.ToDo: return new int[] { };
					case StatusDesignation.InProgress: return new int[] { };
					case StatusDesignation.InReview: return new int[] { (int)StatusDesignation.InProgress,(int)StatusDesignation.Done };
					case StatusDesignation.Done: return new int[] { };
				}
			}
			if (role.Equals("Developer"))
			{
				switch (status)
				{
					case StatusDesignation.ToDo: return new int[] { (int)StatusDesignation.InProgress };
					case StatusDesignation.InProgress: return new int[] { (int)StatusDesignation.InReview };
					case StatusDesignation.InReview: return new int[] { };
					case StatusDesignation.Done: return new int[] { };
				}
			}
			return new int[] { };// todo threw exception - cannot generate statuses
		}
	}
}
