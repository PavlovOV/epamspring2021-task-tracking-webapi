﻿using AutoMapper;
using BLL.Exceptions;
using BLL.Interfaces;
using BLL.Models.Common;
using BLL.Models.Manager;
using DAL;
using DAL.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Services
{
	public class ProjectService: IProjectService
	{

		private readonly IMapper _mapper;
		private readonly IUnitOfWork _uow;
		private readonly UserManager<ApplicationUser> _userManager;


		public ProjectService(IMapperBLLCreator _mapperCreator, IUnitOfWork uow,
			UserManager<ApplicationUser> userManager )
		{
			_userManager = userManager;
			_mapper = _mapperCreator.Mapper;
			_uow = uow;

		}

		public async Task<ProjectDto> GetProject(int id)
		{
			var project = await _uow.Projects.GetByIdAsync(id);
			if (project == null)
				throw new DataNotFoundException("User  not found.") { Status = 400, Value = "User  not found." };
			var result = _mapper.Map<ProjectDto>(project);
			result.Readiness = CalculateProjectReadiness(result);
			return result;
		}

		public async Task<IEnumerable<ProjectDto>> GetProjectsForManager(string managerId)
		{
			var user = await _userManager.FindByIdAsync(managerId);
			if (user == null)
				throw new DataNotFoundException("User  not found.") { Status = 400, Value = "User  not found." };

			var projects = _uow.Projects.FindAllWithDetails().Where(x => x.UserId == managerId);

			var result = _mapper.Map<IEnumerable<ProjectDto>>(projects);
			foreach (var p in result)
				p.Readiness = CalculateProjectReadiness(p);
			return result;
		}

		public async Task<IEnumerable<ProjectDto>> GetProjectsForDeveloper(string developerId)
		{
			var user = await _userManager.FindByIdAsync(developerId);
			if (user == null)
				throw new DataNotFoundException("User  not found.") { Status = 400, Value = "User  not found." };

			var projects = (from p in _uow.Projects.FindAllWithDetails()
						 join o in _uow.Objectives.FindAll() on p.Id equals o.ProjectId
						 where o.UserId == developerId
						 select p).Distinct();

			var result = _mapper.Map<IEnumerable<ProjectDto>>(projects);
			foreach (var p in result)
				p.Readiness = CalculateProjectReadiness(p);
			return result;
		}

		public async Task CreateProjectAsync(CreateProjectDto project, string name)
		{
			var user = await _userManager.FindByNameAsync(name);
			if (user == null)
				throw new DataNotFoundException($"User {name} not found.") { Status = 404, Value = $"User {name} not found." };
			Project p = new Project
			{
				Name = project.Name,
				Description = project.Description,
				UserId = user.Id
			};
			_uow.Projects.Add(p);
			p.Id = 0;
			await _uow.SaveAsync();
			project.Id = p.Id;
		}
		private float CalculateProjectReadiness(ProjectDto project)
		{
			const float eps = 0.00000001f;
			float numerator = 0;
			float denominator = 0;
			foreach (var objective in _uow.Objectives.FindAllWithDetails().Where(x => x.ProjectId == project.Id))
			{
				switch ((StatusDesignation)objective.StatusId)
				{
					case StatusDesignation.ToDo:
						{
							denominator += objective.TimeEstimate;
							break;
						}
					case StatusDesignation.InProgress:
						{
							float duration = objective.WorkLogs.Sum(x => x.TimeLogged);
							if (duration > objective.TimeEstimate)
							{
								numerator += duration;
								denominator += duration;
							}
							else
							{
								numerator += duration;
								denominator += objective.TimeEstimate;
							}

							break;
						}
					case StatusDesignation.InReview:
					case StatusDesignation.Done:
						{
							float duration = objective.WorkLogs.Sum(x => x.TimeLogged);
							numerator += duration;
							denominator += duration;
							break;
						}
				}
			}
			if (Math.Abs(denominator) < eps)
				return 0;
			else
				return numerator / denominator;
		}
	}
}
