﻿using AutoMapper;
using BLL.Exceptions;
using BLL.Interfaces;
using BLL.Models.Common;
using BLL.Models.Manager;
using DAL;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.Models.Developer;
using DAL.Entities;

namespace BLL.Services
{
	public class WorkLogService : IWorkLogService
	{

		private readonly IMapper _mapper;
		private readonly IUnitOfWork _uow;
		private readonly IStatusService _statusService;

		public WorkLogService(IMapperBLLCreator _mapperCreator, IUnitOfWork uow,
			IStatusService statusService)
		{
			_mapper = _mapperCreator.Mapper;
			_uow = uow;
			_statusService = statusService;
		}

		public async Task<WorkLogsWithDetailsForManagerDto> GenerateWorkLogsForManagerWithDetails(string role, int objectiveId)
		{
			var objective = await _uow.Objectives.GetByIdWithDetailsAsync(objectiveId);
			if (objective == null)
				throw new DataNotFoundException("Task  not found.") { Status = 400, Value = "Task  not found." };

			var statuses = await _statusService.GeneratePossibleStatuses(role, (StatusDesignation)objective.StatusId);

			var workLogsDto = _mapper.Map<IEnumerable<WorkLogDto>>(objective.WorkLogs);
			TimeSpan total = new TimeSpan();
			foreach (var item in workLogsDto)
				total = total.Add(item.TimeLogged);
			WorkLogsWithDetailsForManagerDto result = new WorkLogsWithDetailsForManagerDto
			{
				Project = _mapper.Map<ProjectDto>(objective.Project),
				Objective = _mapper.Map<ObjectiveForManagerDto>(objective),
				WorkLogs = workLogsDto,
				TimeLogged = total,
				PossibleStatuses = statuses,
			};

			return result;
		}

		public async Task<WorkLogsWithDetailsForDeveloperDto> GenerateWorkLogsForDeveloperWithDetails(string role, int objectiveId)
		{
			var objective = await _uow.Objectives.GetByIdWithDetailsAsync(objectiveId);
			if (objective == null)
				throw new DataNotFoundException("Task  not found.") { Status = 400, Value = "Task  not found." };

			var statuses = await _statusService.GeneratePossibleStatuses(role, (StatusDesignation)objective.StatusId);

			var workLogsDto = _mapper.Map<IEnumerable<WorkLogDto>>(objective.WorkLogs);
			TimeSpan total = new TimeSpan();
			foreach (var item in workLogsDto)
				total = total.Add(item.TimeLogged);
			WorkLogsWithDetailsForDeveloperDto result = new WorkLogsWithDetailsForDeveloperDto
			{
				Project = _mapper.Map<ProjectDto>(await _uow.Projects.GetByIdWithDetailsAsync(objective.ProjectId)),
				Objective = _mapper.Map<ObjectiveForDeveloperDto>(objective),
				WorkLogs = workLogsDto,
				TimeLogged = total,
				PossibleStatuses = statuses,
				IsAllowedToAddLogWork = ((StatusDesignation)objective.StatusId == StatusDesignation.InProgress)
			};
			return result;
		}

		public async Task CreateWorkLogAsync(CreateWorkLogDto workLogDto)
		{
			WorkLog workLog = _mapper.Map<WorkLog>(workLogDto);
			_uow.WorkLogs.Add(workLog);
			workLog.Id = 0;
			await _uow.SaveAsync();
		}
	}
}
