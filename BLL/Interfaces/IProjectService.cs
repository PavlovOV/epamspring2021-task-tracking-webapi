﻿using BLL.Models.Manager;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
	public interface IProjectService
	{
		Task<IEnumerable<ProjectDto>> GetProjectsForManager(string managerId);
		Task<IEnumerable<ProjectDto>> GetProjectsForDeveloper(string developerId);
		Task<ProjectDto> GetProject(int id);
		Task CreateProjectAsync(CreateProjectDto project, string name);
	}
}
