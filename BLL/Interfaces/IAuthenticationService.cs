﻿using BLL.Models.Account;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
	public interface IAuthenticationService
	{
		Task<AuthenticationResultDto> Login(UserLoginDto userModel);
		Task<AuthenticationResultDto> Register(UserRegisterDto userModel);
	}
}
