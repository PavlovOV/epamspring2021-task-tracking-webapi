﻿using BLL.Models.Common;
using BLL.Models.Manager;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
	public interface IStatusService
	{
		Task<IEnumerable<StatusDto>> GeneratePossibleStatuses(string role, StatusDesignation status );
		void SetInitialStatus(CreateObjectiveDto objective);
	}
}
