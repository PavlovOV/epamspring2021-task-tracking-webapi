﻿using BLL.Models;
using BLL.Models.Admin;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
	public interface IIdentityService
	{
		Task<(IEnumerable<UserForAdminDto>, PagingInfo)> GetUsersForPpecificRole(DataForUserSearchFilterDto filterSort, string roleId, int currentPage);
		IEnumerable<RoleDto> GetRoles();

	}
}
