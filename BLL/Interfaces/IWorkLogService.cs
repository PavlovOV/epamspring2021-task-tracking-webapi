﻿using BLL.Models.Developer;
using BLL.Models.Manager;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
	public interface IWorkLogService
	{
		Task<WorkLogsWithDetailsForManagerDto> GenerateWorkLogsForManagerWithDetails(string role, int objectiveId);
		Task<WorkLogsWithDetailsForDeveloperDto> GenerateWorkLogsForDeveloperWithDetails(string role, int objectiveId);
		Task CreateWorkLogAsync(CreateWorkLogDto workLogDto);
	}
}
