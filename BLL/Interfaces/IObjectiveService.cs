﻿using BLL.Models.Developer;
using BLL.Models.Manager;
using BLL.Models.Notification;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
	public interface IObjectiveService
	{
		Task CreateObjectiveAsync(CreateObjectiveDto objectiveDto, string reporterName, EmailDistributionDto emailInfo);
		Task ChangeObjectiveStatus(string role, int objectiveId, int newStatusId);
		Task AssignDeveloper(AssignDeveloperDto assignDeveloperDto, string reporterName, EmailDistributionDto emailInfo);
		Task<IEnumerable<ObjectiveForManagerDto>> GetObjectivesForManager(int projectId);
		Task<IEnumerable<ObjectiveForDeveloperDto>> GetObjectivesForDeveloper(int projectId, string name);
		Task<IEnumerable<DeveloperDto>> GetDevelopers();
	}
}
