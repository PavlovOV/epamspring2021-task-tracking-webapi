﻿using BLL.Models.Manager;
using BLL.Models.Notification;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
	public interface INotificationService
	{
		Task EmailingAboutTaskAssign(ObjectiveForManagerDto objective, string reporterName, EmailDistributionDto emailInfo);
	}
}
