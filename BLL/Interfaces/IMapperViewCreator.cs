﻿using AutoMapper;

namespace BLL.Interfaces
{
	public interface IMapperBLLCreator
	{
		IMapper Mapper { get; }
	}
}
