﻿
namespace BLL.Models.Manager
{
	public class AssignDeveloperDto
	{
		public string DeveloperId { get; set; }
		public int ObjectiveId { get; set; }
	}
}
