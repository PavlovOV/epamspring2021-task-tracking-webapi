﻿

namespace BLL.Models.Manager
{
	public class DeveloperDto
	{
		public string Id { get; set; }
		public string Name { get; set; }
	}
}
