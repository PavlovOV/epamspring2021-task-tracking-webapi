﻿using System;

namespace BLL.Models.Manager
{
	public class WorkLogDto
	{
		public int Id { get; set; }
		public string Description { get; set; }
		public TimeSpan TimeLogged { get; set; }

	}
}
