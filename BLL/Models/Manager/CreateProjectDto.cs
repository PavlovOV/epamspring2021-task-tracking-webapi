﻿
namespace BLL.Models.Manager
{
	public class CreateProjectDto
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
	}
}
