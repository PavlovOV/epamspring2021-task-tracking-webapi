﻿using System;

namespace BLL.Models.Manager
{
	public class CreateObjectiveDto
	{

		public string Name { get; set; }

		public string Description { get; set; }

		public int StatusId { get; set; }

		public int ProjectId { get; set; }

		public string UserId { get; set; }

		public TimeSpan TimeEstimate { get; set; }
	}
}
