﻿using BLL.Models.Common;
using System;
using System.Collections.Generic;

namespace BLL.Models.Manager
{
	public class WorkLogsWithDetailsForManagerDto
	{
		public ProjectDto Project { get; set; }
		public ObjectiveForManagerDto Objective { get; set; }
		public IEnumerable<WorkLogDto> WorkLogs { get; set; }
		public IEnumerable<StatusDto> PossibleStatuses { get; set; }
		public TimeSpan TimeLogged { get; set; }

	}
}
