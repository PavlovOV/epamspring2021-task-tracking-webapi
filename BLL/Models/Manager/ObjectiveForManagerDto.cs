﻿using BLL.Models.Common;
using System;

namespace BLL.Models.Manager
{
	public class ObjectiveForManagerDto
	{
		public string Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public StatusDto Status { get; set; }
		public int ProjectId { get; set; }
		public string UserId { get; set; }
		public string UserName { get; set; }
		public TimeSpan TimeEstimate { get; set; }

	}
}
