﻿namespace BLL.Models.Manager
{
	public class ProjectDto
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public string UserId { get; set; }
		public string UserName { get; set; }

		public float Readiness { get; set; }

	}
}
