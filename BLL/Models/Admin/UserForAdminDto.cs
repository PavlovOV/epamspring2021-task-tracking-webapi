﻿
namespace BLL.Models.Admin
{
	public class UserForAdminDto
	{
		public string Id { get; set; }
		public string Name { get; set; }
		public string Email { get; set; }
	}
}
