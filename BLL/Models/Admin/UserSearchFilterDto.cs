﻿namespace BLL.Models.Admin
{
	public class DataForUserSearchFilterDto
	{
		public string Name { get; set; }
		public SortingOrder NameOrder { get; set; }

	}
}
