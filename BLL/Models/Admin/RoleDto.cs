﻿
namespace BLL.Models.Admin
{
	public class RoleDto
	{
		public string Id { get; set; }
		public string Name { get; set; }
	}
}
