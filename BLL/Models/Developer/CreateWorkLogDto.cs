﻿using System;

namespace BLL.Models.Developer
{
	public class CreateWorkLogDto
	{
		public string Description { get; set; }

		public int ObjectiveId { get; set; }

		public TimeSpan TimeLogged { get; set; }
	}
}
