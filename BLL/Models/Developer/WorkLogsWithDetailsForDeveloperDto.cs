﻿using BLL.Models.Common;
using BLL.Models.Manager;
using System;
using System.Collections.Generic;

namespace BLL.Models.Developer
{
	public class WorkLogsWithDetailsForDeveloperDto
	{
		public ProjectDto Project { get; set; }
		public ObjectiveForDeveloperDto Objective { get; set; }
		public IEnumerable<WorkLogDto> WorkLogs { get; set; }
		public IEnumerable<StatusDto> PossibleStatuses { get; set; }
		public TimeSpan TimeLogged { get; set; }
		public bool IsAllowedToAddLogWork { get; set; }
	}
}
