﻿using AutoMapper;
using BLL.Models.Admin;
using BLL.Models.Common;
using BLL.Models.Developer;
using BLL.Models.Manager;
using DAL.Entities;
using Microsoft.AspNetCore.Identity;
using System;

namespace BLL.Models.Mapper
{
	class AutomapperBLLProfile : Profile
	{
		public AutomapperBLLProfile()
		{

			CreateMap<ApplicationUser, UserForAdminDto>()
				.ForMember(c => c.Name, c => c.MapFrom(p => p.UserName))
				.ForMember(c => c.Email, c => c.MapFrom(p => p.Email));
			CreateMap<Project, ProjectDto>()
				.ForMember(c => c.UserName, c => c.MapFrom(p => p.ApplicationUser.UserName));
			CreateMap<ProjectDto, Project>();

			CreateMap<IdentityRole, RoleDto>()
				.ForMember(c => c.Name, c => c.MapFrom(p => p.Name));

			CreateMap<Objective, ObjectiveForManagerDto>()
				.ForMember(c => c.TimeEstimate, c => c.MapFrom(p => new TimeSpan(p.TimeEstimate)))
				.ForMember(c => c.UserName, c => c.MapFrom(p => p.ApplicationUser.UserName));

			CreateMap<CreateObjectiveDto, Objective>()
			.ForMember(c => c.TimeEstimate, c => c.MapFrom(p => p.TimeEstimate.Ticks));
			CreateMap<ApplicationUser, DeveloperDto>()
				.ForMember(c => c.Name, c => c.MapFrom(p => p.UserName));

			CreateMap<WorkLog, WorkLogDto>()
				.ForMember(c => c.TimeLogged, c => c.MapFrom(p => new TimeSpan(p.TimeLogged)));

			CreateMap <StatusDto,Status>().ReverseMap();

			CreateMap<Objective, ObjectiveForDeveloperDto>()
				.ForMember(c => c.TimeEstimate, c => c.MapFrom(p => new TimeSpan(p.TimeEstimate)));

			CreateMap<CreateWorkLogDto, WorkLog>()
				.ForMember(c => c.TimeLogged, c => c.MapFrom(p => p.TimeLogged.Ticks));

		}
	}
}
