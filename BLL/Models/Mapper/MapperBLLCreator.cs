﻿using AutoMapper;
using BLL.Interfaces;

namespace BLL.Models.Mapper
{

	public class MapperBLLCreator : IMapperBLLCreator
	{
		private readonly IMapper _mapper;
		public IMapper Mapper { get { return _mapper; } }
		public MapperBLLCreator()
		{
			var myProfile = new AutomapperBLLProfile();
			var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));

			_mapper = new AutoMapper.Mapper(configuration);
		}

	}
}
