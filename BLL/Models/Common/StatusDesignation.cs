﻿
namespace BLL.Models.Common
{

	public enum StatusDesignation
	{
		ToDo = 1,
		InProgress = 2,
		InReview = 3,
		Done = 4,
	}
}
