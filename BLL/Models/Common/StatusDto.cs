﻿namespace BLL.Models.Common
{
	public class StatusDto
	{
		public int Id { get; set; }
		public string Name { get; set; }
	}
}
