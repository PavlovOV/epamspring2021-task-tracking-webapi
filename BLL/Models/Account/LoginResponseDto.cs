﻿
namespace BLL.Models.Account
{
	public class LoginResponseDto
	{
		public string Token { get; set; }
		public string UserName { get; set; }
		public string[] Roles { get; set; }
		public string UserId { get; set; }
	}
}
