﻿
namespace BLL.Models.Account
{
	public class UserLoginDto
	{
		public string Name { get; set; }
		public string Password { get; set; }
	}
}
