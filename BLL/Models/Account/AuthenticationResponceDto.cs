﻿using System.Collections.Generic;

namespace BLL.Models.Account
{
	public class AuthenticationResultDto
	{
		public LoginResponseDto Response { get; set; }
		public List<string> Errors { get; set; }
		public bool Succedeed { get; set; }


	}
}
