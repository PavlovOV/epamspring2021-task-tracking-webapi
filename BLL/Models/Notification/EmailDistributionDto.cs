﻿
namespace BLL.Models.Notification
{
	public class EmailDistributionDto
	{
		public string EmailAccount { get; set; }
		public string EmailPassword { get; set; }
		public string SMTP_HOST { get; set; }
		public string SMTP_PORT { get; set; }
	}
}
