﻿using System;
using System.Runtime.Serialization;

namespace BLL.Exceptions
{
	public class DataNotFoundException : Exception
	{
		public int Status { get; set; } = 500;

		public string Value { get; set; }
		public DataNotFoundException() { }

		public DataNotFoundException(string message) { }

		protected DataNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			base.GetObjectData(info, context);
		}
	}
}
