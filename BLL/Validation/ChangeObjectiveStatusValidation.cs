﻿using BLL.Interfaces;
using BLL.Models.Common;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Validation
{
	public static class ChangeObjectiveStatus
	{
		public static async Task<bool> IsValid(IStatusService _statusService, string role, int oldStatusId, int newStatusId)
		{
			var possibleStatusesId = (await _statusService.GeneratePossibleStatuses(role, (StatusDesignation)oldStatusId)).Select(x => x.Id);
			return possibleStatusesId.Contains(newStatusId);
		}
	}
}
