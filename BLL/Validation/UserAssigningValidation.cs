﻿using DAL.Entities;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace BLL.Validation
{
	public static class UserAssigning
	{
		public static async Task<bool> IsValid(UserManager<ApplicationUser> _userManager, string userId)
		{
			var user = await _userManager.FindByIdAsync(userId);
			var userRoles = await _userManager.GetRolesAsync(user);
			return userRoles.Contains("Developer");
		}
	}
}
